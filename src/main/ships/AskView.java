package ships;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import javax.swing.ButtonGroup;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import ships.controller.Controller;
import ships.controller.LocalProtocol;
import ships.controller.MyServer;
import ships.controller.RemoteProtocol;
import ships.events.ClientEvent;
import ships.model.Model;
import ships.view.View;

/**
 * Widok, który pyta użytkownika, czy chce uruchomić serwer, czy też połączyć się z nim.
 */
public class AskView {

    /**
     * Wyświetla okno dialogowe.
     */
    public AskView() {
        // Opis okienka.
        JLabel description = new JLabel("Wybierz co chcesz zrobić?");

        // Pole z adresem ip.
        final JTextField ipAddressField = new JTextField("localhost");
        ipAddressField.setEnabled(false);

        // Przycisk do uruchomienia serwera.
        JRadioButton serverButton = new JRadioButton("Uruchom serwer");
        serverButton.setSelected(true);
        serverButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ipAddressField.setEnabled(false);
            }
        });

        // Przycisk do połączenia z serwerem.
        JRadioButton clientButton = new JRadioButton("Połącz się z serwerem");
        clientButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ipAddressField.setEnabled(true);
            }
        });

        // Wykluczanie przycisków.
        ButtonGroup buttonGroup = new ButtonGroup();
        buttonGroup.add(serverButton);
        buttonGroup.add(clientButton);

        JComponent[] inputs = new JComponent[] {
                description,
                serverButton,
                clientButton,
                ipAddressField
        };
        int n = JOptionPane.showConfirmDialog(
                null,
                inputs,
                "Statki - serwer czy klient?",
                JOptionPane.OK_CANCEL_OPTION);
        if (n == 0) {
            if (serverButton.isSelected()) {
                runServer();
            } else {
                String ipAddress = ipAddressField.getText();
                connectToServer(ipAddress);
            }
        }
    }

    /**
     * Tworzy serwer, a wraz z nim potrzebne obiekty.
     * M. in. protokół, model, kontroler i kolejkę zdarzeń klientów.
     */
    protected void runServer() {
        BlockingQueue<ClientEvent> eventQueue = new LinkedBlockingQueue<>();
        LocalProtocol protocol = new LocalProtocol(eventQueue);
        View view = new View(protocol);
        protocol.addObserver(view);
        Model model = new Model();
        MyServer server = new MyServer(eventQueue);
        server.addObserver(protocol);
        server.setConnectionErrorHandler(view.getConnectionErrorHandler());
        Thread serverThread = new Thread(server);
        serverThread.start();
        Controller controller = new Controller(model, server, eventQueue);
        model.addObserver(controller);
        Thread controllerThread = new Thread(controller);
        controllerThread.start();
    }

    /**
     * Łączy się z istniejącym serwerem.
     * @param ipAddress adres ip serwera
     */
    protected void connectToServer(String ipAddress) {
        RemoteProtocol protocol = new RemoteProtocol(ipAddress);
        View view = new View(protocol);
        protocol.addObserver(view);
        protocol.setConnectionErrorHandler(
                view.getConnectionErrorHandler());
        Thread protocolThread = new Thread(protocol);
        protocolThread.start();
    }
}
