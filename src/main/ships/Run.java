package ships;

/**
 * Klasa Uruchom.
 * Tworzy obiekt widoku, który pyta gracza, czy chce uruchomić serwer
 * czy też połączyć sie z istniejącym.
 */
public class Run {

    public static void main(String[] args) {
        new AskView();
    }
}
