package ships.controller;

/**
 * Interfejs do obsługi wyjątku spowodowanego problemem z połączeniem.
 */
public interface ConnectionErrorHandler {

    void uncaughtException(Throwable e);
}