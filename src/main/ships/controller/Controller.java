package ships.controller;

import java.util.HashMap;
import java.util.Map;
import java.util.Observable;
import java.util.Observer;
import java.util.concurrent.BlockingQueue;
import ships.events.AllShipsPlacedEvent;
import ships.events.ClientEvent;
import ships.events.LostGameEvent;
import ships.events.ModelChangeEvent;
import ships.events.NewGameAgreeEvent;
import ships.events.NewGameRejectEvent;
import ships.events.NewGameRejectReceivedEvent;
import ships.events.NewGameRequestEvent;
import ships.events.NewGameRequestReceivedEvent;
import ships.events.OpponentTurnEvent;
import ships.events.PlaceShipEvent;
import ships.events.PlaceShipFailedEvent;
import ships.events.PlaceShipSuccessfulEvent;
import ships.events.PlayerTurnEvent;
import ships.events.PlayerType;
import ships.events.RemotePlayerConnectedEvent;
import ships.events.ShotEvent;
import ships.events.StartGameEvent;
import ships.events.WinGameEvent;
import ships.model.Coordinates;
import ships.model.Model;
import ships.model.ModelChange;
import ships.model.Orientation;
import ships.model.PlayersModelChange;
import ships.model.ShipType;

/**
 * Klasa głównego kontrolera obsługującego zdarzenia wysyłane przez graczy
 * oraz pilnującego przebiegu gry.
 * Kontroler powinien być obserwatorem modelu, aby mógł być powiadamianym
 * o zmianach w modelu.
 */
public class Controller implements Observer, Runnable{

    protected final Model model;
    protected final Server server;
    /**
     * Kolejka zdarzeń otrzymanych od klientów.
     */
    protected final BlockingQueue<ClientEvent> eventQueue;
    /**
     * Mapa zdarzenie klienta-reakcja dla zdarzeń otrzymanych
     * od lokalnego gracza.
     */
    protected Map<Class<? extends ClientEvent>, EventHandler> localPlayerMap;
    /**
     * Mapa zdarzenie klienta-reakcja dla zdarzeń otrzymanych
     * od zdalnego gracza.
     */
    protected Map<Class<? extends ClientEvent>, EventHandler> remotePlayerMap;
    protected PlayerTurn playerTurn;

    /**
     * Tworzy obiekt kontrolera.
     * @param model model
     * @param server serwer
     * @param eventQueue kolejka zdarzeń wysłanych przez graczy
     */
    public Controller(Model model, Server server,
                      BlockingQueue<ClientEvent> eventQueue) {
        this.model = model;
        this.server = server;
        this.eventQueue = eventQueue;
        fillMaps();
        playerTurn = null;
    }

    /**
     * Obsługuje powiadomienia modelu.
     * Otrzymaną zmianę modelu wysyła do dwóch klientów.
     */
    @Override
    public void update(Observable obj, Object arg) {
        if (obj instanceof Model && arg instanceof PlayersModelChange) {
            PlayersModelChange playersModelChange =
                    (PlayersModelChange) arg;
            ModelChangeEvent localPlayerModelChangeEvent =
                    new ModelChangeEvent(
                            playersModelChange.getLocalPlayerModelChange());
            server.sendEventToLocalPlayer(localPlayerModelChangeEvent);
            ModelChangeEvent remotePlayerModelChangeEvent =
                    new ModelChangeEvent(
                            playersModelChange.getRemotePlayerModelChange());
            server.sendEventToRemotePlayer(remotePlayerModelChangeEvent);
        }
    }

    /**
     * Czyta zdarzenia od graczy i je obsługuje.
     */
    @Override
    public void run() {
        while (true) {
            ClientEvent event = null;
            try {
                event = eventQueue.take();
            } catch (InterruptedException e) {
            }
            handleEvent(event);
        }
    }

    /**
     * Obsługuje zdarzenie od gracza.
     * Wybiera odpowiedni obiekt do obsługi zdarzenia w zależności
     * od rodzaju gracza.
     * @param event zdarzenie od gracza
     */
    protected void handleEvent(ClientEvent event) {
        EventHandler eventHandler;
        if (event.getPlayerType() == PlayerType.LOCAL_PLAYER) {
            eventHandler = localPlayerMap.get(event.getClass());
        } else {
            eventHandler = remotePlayerMap.get(event.getClass());
        }
        if (eventHandler != null) {
            eventHandler.handleEvent(event);
        }
    }

    /**
     * Tworzy 2 mapy zdarzenie klienta-reakcja na nie.
     */
    protected void fillMaps() {
        // Nie można użyć TreeMap, gdyż Class nie implementuje Comparable.
        localPlayerMap = new HashMap<>();
        remotePlayerMap = new HashMap<>();

        // Obsługuje zdarzenie nawiązania połączenia ze zdalnym graczem.
        EventHandler remotePlayerConnectedEventHandler = new EventHandler() {
            @Override
            public void handleEvent(ClientEvent clientEvent) {
                server.sendEventToLocalPlayer(new StartGameEvent());
                server.sendEventToRemotePlayer(new StartGameEvent());
            }
        };
        remotePlayerMap.put(RemotePlayerConnectedEvent.class,
                remotePlayerConnectedEventHandler);

        // Obsługuje zdarzenie oddania strzału.
        EventHandler localPlayerShotEventHandler = new EventHandler() {
            @Override
            public void handleEvent(ClientEvent clientEvent) {
                ShotEvent event = (ShotEvent) clientEvent;
                if (!(playerTurn == PlayerTurn.LOCAL_PLAYER_TURN)) {
                    return;
                }
                boolean shotResult =
                        model.handleLocalPlayerShot(event.getShotCoordinates());
                if (!shotResult) {
                    server.sendEventToLocalPlayer(new OpponentTurnEvent());
                    server.sendEventToRemotePlayer(new PlayerTurnEvent());
                    playerTurn = PlayerTurn.REMOTE_PLAYER_TURN;
                } else if (model.isLocalPlayerWinner()) {
                    ModelChange modelChange =
                            model.getNotDestroyedShips()
                                    .getRemotePlayerModelChange();
                    server.sendEventToRemotePlayer(
                            new ModelChangeEvent(modelChange));
                    server.sendEventToLocalPlayer(new WinGameEvent());
                    server.sendEventToRemotePlayer(new LostGameEvent());
                }
            }
        };
        EventHandler remotePlayerShotEventHandler = new EventHandler() {
            @Override
            public void handleEvent(ClientEvent clientEvent) {
                ShotEvent event = (ShotEvent) clientEvent;
                if (!(playerTurn == PlayerTurn.REMOTE_PLAYER_TURN)) {
                    return;
                }
                boolean shotResult =
                        model.handleRemotePlayerShot(event.getShotCoordinates());
                if (!shotResult) {
                    server.sendEventToLocalPlayer(new PlayerTurnEvent());
                    server.sendEventToRemotePlayer(new OpponentTurnEvent());
                    playerTurn = PlayerTurn.LOCAL_PLAYER_TURN;
                } else if (model.isRemotePlayerWinner()) {
                    ModelChange modelChange =
                            model.getNotDestroyedShips()
                                    .getLocalPlayerModelChange();
                    server.sendEventToLocalPlayer(
                            new ModelChangeEvent(modelChange));
                    server.sendEventToLocalPlayer(new LostGameEvent());
                    server.sendEventToRemotePlayer(new WinGameEvent());
                }
            }
        };
        localPlayerMap.put(ShotEvent.class, localPlayerShotEventHandler);
        remotePlayerMap.put(ShotEvent.class, remotePlayerShotEventHandler);

        // Obsługuje zdarzenie ustawienia statku.
        EventHandler localPlayerPlaceShipEventHandler = new EventHandler() {
            @Override
            public void handleEvent(ClientEvent clientEvent) {
                PlaceShipEvent event = (PlaceShipEvent) clientEvent;
                ShipType shipType = event.getShipType();
                Orientation orientation = event.getOrientation();
                Coordinates coordinates = event.getCoordinates();
                boolean shipPlaced = model.placeLocalPlayerShip(
                        shipType, orientation, coordinates);
                if (!shipPlaced) {
                    server.sendEventToLocalPlayer(
                            new PlaceShipFailedEvent(shipType));
                } else {
                    server.sendEventToLocalPlayer(
                            new PlaceShipSuccessfulEvent(shipType));
                    if (model.areLocalPlayerShipsPlaced()) {
                        server.sendEventToLocalPlayer(new AllShipsPlacedEvent());
                    }
                    if (model.arePlayersShipsPlaced()) {
                        playerTurn = PlayerTurn.REMOTE_PLAYER_TURN;
                        server.sendEventToLocalPlayer(new OpponentTurnEvent());
                        server.sendEventToRemotePlayer(new PlayerTurnEvent());
                    }
                }
            }
        };
        EventHandler remotePlayerPlaceShipEventHandler = new EventHandler() {
            @Override
            public void handleEvent(ClientEvent clientEvent) {
                PlaceShipEvent event = (PlaceShipEvent) clientEvent;
                ShipType shipType = event.getShipType();
                Orientation orientation = event.getOrientation();
                Coordinates coordinates = event.getCoordinates();
                boolean shipPlaced = model.placeRemotePlayerShip(
                        shipType, orientation, coordinates);
                if (!shipPlaced) {
                    server.sendEventToRemotePlayer(
                            new PlaceShipFailedEvent(shipType));
                } else {
                    server.sendEventToRemotePlayer(
                            new PlaceShipSuccessfulEvent(shipType));
                    if (model.areRemotePlayerShipsPlaced()) {
                        server.sendEventToRemotePlayer(new AllShipsPlacedEvent());
                    }
                    if (model.arePlayersShipsPlaced()) {
                        playerTurn = PlayerTurn.LOCAL_PLAYER_TURN;
                        server.sendEventToLocalPlayer(new PlayerTurnEvent());
                        server.sendEventToRemotePlayer(new OpponentTurnEvent());
                    }
                }
            }
        };
        localPlayerMap.put(PlaceShipEvent.class, localPlayerPlaceShipEventHandler);
        remotePlayerMap.put(PlaceShipEvent.class, remotePlayerPlaceShipEventHandler);

        // Obsługa zapytania o rozpoczęcie nowej gry.
        EventHandler localPlayerNewGameRequestEventHandler = new EventHandler() {
            @Override
            public void handleEvent(ClientEvent event) {
                server.sendEventToRemotePlayer(new NewGameRequestReceivedEvent());
            }
        };
        EventHandler remotePlayerNewGameRequestEventHandler = new EventHandler() {
            @Override
            public void handleEvent(ClientEvent event) {
                server.sendEventToLocalPlayer(new NewGameRequestReceivedEvent());
            }
        };
        localPlayerMap.put(NewGameRequestEvent.class,
                localPlayerNewGameRequestEventHandler);
        remotePlayerMap.put(NewGameRequestEvent.class,
                remotePlayerNewGameRequestEventHandler);

        // Obsługa zgody na nową grę.
        EventHandler localPlayerNewGameAgreeEventHandler = new EventHandler() {
            @Override
            public void handleEvent(ClientEvent event) {
                model.reset();
                playerTurn = null;
                server.sendEventToLocalPlayer(new StartGameEvent());
                server.sendEventToRemotePlayer(new StartGameEvent());
            }
        };
        EventHandler remotePlayerNewGameAgreeEventHandler = new EventHandler() {
            @Override
            public void handleEvent(ClientEvent event) {
                model.reset();
                playerTurn = null;
                server.sendEventToLocalPlayer(new StartGameEvent());
                server.sendEventToRemotePlayer(new StartGameEvent());
            }
        };
        localPlayerMap.put(NewGameAgreeEvent.class,
                localPlayerNewGameAgreeEventHandler);
        remotePlayerMap.put(NewGameAgreeEvent.class,
                remotePlayerNewGameAgreeEventHandler);

        // Obsługa odmowy na nową grę.
        EventHandler localPlayerNewGameRejectEventHandler = new EventHandler() {
            @Override
            public void handleEvent(ClientEvent event) {
                server.sendEventToRemotePlayer(new NewGameRejectReceivedEvent());
            }
        };
        EventHandler remotePlayerNewGameRejectEventHandler = new EventHandler() {
            @Override
            public void handleEvent(ClientEvent event) {
                server.sendEventToLocalPlayer(new NewGameRejectReceivedEvent());
            }
        };
        localPlayerMap.put(NewGameRejectEvent.class,
                localPlayerNewGameRejectEventHandler);
        remotePlayerMap.put(NewGameRejectEvent.class,
                remotePlayerNewGameRejectEventHandler);
    }

    /**
     * Interfejs obiektu do obsługi zdarzeń klienta.
     */
    protected interface EventHandler {
        void handleEvent(ClientEvent event);
    }

    /**
     * Typ wyliczeniowy wskazujący aktualną turę.
     */
    protected enum PlayerTurn {
        LOCAL_PLAYER_TURN, REMOTE_PLAYER_TURN
    }
}
