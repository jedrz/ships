package ships.controller;

import java.util.Observable;
import java.util.Observer;
import java.util.concurrent.BlockingQueue;
import ships.events.ClientEvent;
import ships.events.ControllerEvent;
import ships.events.PlayerType;

/**
 * Protokół umożliwiający komunikację z kontrolerem w ramach jednego
 * programu za pomocą kolejki blokującej.
 * Powinien być obserwatorem serwera, aby móc powiadomić widok
 * o nowym zdarzeniu.
 */
public class LocalProtocol extends Protocol implements Observer {

    /**
     * Kolejka, w której są umieszczane zdarzenia wygenerowane
     * przez gracza.
     */
    protected final BlockingQueue<ClientEvent> eventQueue;

    /**
     * Tworzy obiekt kontrolera, który będzie używać podanej kolejki
     * do komunikacji.
     */
    public LocalProtocol(BlockingQueue<ClientEvent> eventQueue) {
        this.eventQueue = eventQueue;
    }

    /**
     * Powiadamia widok o zdarzeniu otrzymanym od kontrolera.
     */
    @Override
    public void update(Observable obj, Object arg) {
        if (obj instanceof MyServer && arg instanceof ControllerEvent) {
            ControllerEvent event = (ControllerEvent) arg;
            notifyOfEvent(event);
        }
    }

    /**
     * Wysyła zdarzenie klienta do kontrolera.
     * Zdarzenie jest umieszczane w kolejce.
     */
    @Override
    public void sendEvent(ClientEvent event) {
        // Wstrzyknij informację o graczu.
        event.setPlayerType(PlayerType.LOCAL_PLAYER);
        try {
            eventQueue.put(event);
        } catch (InterruptedException e) {
        }
    }
}
