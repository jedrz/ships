package ships.controller;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Observable;
import java.util.concurrent.BlockingQueue;
import ships.events.ClientEvent;
import ships.events.ControllerEvent;
import ships.events.RemotePlayerConnectedEvent;

/**
 * Klasa serwera implementująca interfejs {@link Server}.
 * Otrzymane zdarzenia od zdalnego klienta umieszcza w kolejce.
 * {@link LocalProtocol} powinien być obserwatorem obiektu serwera,
 * aby móc odbiera zdarzenia kontrolera.
 * Serwer należy uruchomić jako wątek, aby móc odbierać zdarzenia
 * od zdalnego klienta.
 */
public class MyServer extends Observable implements Server, Runnable {

    /**
     * Numer portu, którego należy użyć łącząc się z serwerem.
     */
    public static final int PORT_NUMBER = 1234;
    /**
     * Kolejka zdarzeń, w której serwer umieszcza zdarzenia otrzymane
     * od zdalnego klienta.
     */
    protected final BlockingQueue<ClientEvent> eventQueue;
    protected Socket clientSocket;
    protected ServerSocket serverSocket;
    protected ObjectInputStream in;
    protected ObjectOutputStream out;
    /**
     * Obiekt, który zostaje powiadomiony o wyjątku.
     */
    protected ConnectionErrorHandler connectionErrorHandler;

    /**
     * Tworzy obiekt serwera.
     * @param eventQueue kolejka, do której wkładane są 
     */
    public MyServer(BlockingQueue<ClientEvent> eventQueue) {
        this.eventQueue = eventQueue;
        connectionErrorHandler = null;
    }

    /**
     * Ustawia obiekt do obsługi wyjątku.
     */
    public void setConnectionErrorHandler(ConnectionErrorHandler connectionErrorHandler) {
        this.connectionErrorHandler = connectionErrorHandler;
    }

    /**
     * Odbiera zdarzenia od zdalnego klienta.
     * Na początku nawiązuje z nim połączenie, po czym informuje kontroler
     * o tym, że zdalny gracz połączył się.
     */
    @Override
    public void run() {
        try {
            serverSocket = new ServerSocket(PORT_NUMBER);
            clientSocket = serverSocket.accept();
            out = new ObjectOutputStream(clientSocket.getOutputStream());
            in = new ObjectInputStream(clientSocket.getInputStream());
            RemotePlayerConnectedEvent event =
                    new RemotePlayerConnectedEvent();
            putEventInQueue(event);
        } catch (IOException e) {
            if (connectionErrorHandler != null) {
                connectionErrorHandler.uncaughtException(e);
            }
        }

        while (true) {
            try {
                ClientEvent event = (ClientEvent) in.readObject();
                putEventInQueue(event);
            } catch (IOException | ClassNotFoundException e) {
                if (connectionErrorHandler != null) {
                    connectionErrorHandler.uncaughtException(e);
                }
                break;
            }
        }
    }

    /**
     * Wysyła zdarzenie do lokalnego gracza.
     * Powiadamia o zdarzeniu lokalny protokół.
     */
    @Override
    synchronized public void sendEventToLocalPlayer(ControllerEvent event) {
        setChanged();
        notifyObservers(event);
    }

    /**
     * Wysyła zdarzenie do zdalnego gracza.
     * Wysyła je poprzez socket.
     */
    @Override
    synchronized public void sendEventToRemotePlayer(ControllerEvent event) {
        try {
            out.writeObject(event);
            out.flush();
        } catch (IOException e) {
        }
    }

    private void putEventInQueue(ClientEvent event) {
        try {
            eventQueue.put(event);
        } catch (InterruptedException e) {
        }
    }

    @Override
    protected void finalize() throws Throwable {
        try {
            out.close();
            in.close();
            clientSocket.close();
            serverSocket.close();
        } catch (IOException e) {
        } finally {
            super.finalize();
        }
    }
}
