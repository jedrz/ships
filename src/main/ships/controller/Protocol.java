package ships.controller;

import java.util.Observable;
import ships.events.ClientEvent;
import ships.events.ControllerEvent;

/**
 * Abstrakcyjna klasa protokołu.
 * Obserwator protokołu - widok, jest powiadamiany o zdarzenia otrzymanych
 * od kontrolera.
 * Należy zaimplementować sposób wysyłania zdarzeń do kontrolera
 * oraz odbierania.
 */
public abstract class Protocol extends Observable {

    /**
     * Umożliwia wysłanie zdarzenia klienta do kontrolera.
     */
    public abstract void sendEvent(ClientEvent clientEvent);

    /**
     * Powiadamia obserwatora - widok, o zdarzenia od kontrolera.
     */
    protected void notifyOfEvent(ControllerEvent event) {
        setChanged();
        notifyObservers(event);
    }
}
