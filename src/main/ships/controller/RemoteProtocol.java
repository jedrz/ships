package ships.controller;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import ships.events.ClientEvent;
import ships.events.ControllerEvent;
import ships.events.PlayerType;

/**
 * Protokół umożliwiający komunikację z kontrolerem poprzez socket.
 * Należy go uruchomić jako wątek, aby nawiązać połączenie z serwerem.
 */
public class RemoteProtocol extends Protocol implements Runnable {

    protected Socket socket;
    protected ObjectInputStream in;
    protected ObjectOutputStream out;
    protected String serverIpAddress;
    /**
     * Obiekt, który zostaje powiadomiony o nieobsłużonym wyjątku.
     */
    protected ConnectionErrorHandler connectionErrorHandler;

    /**
     * Tworzy obiekt protokołu.
     * @param serverIpAddress adres ip serwera/kontrolera
     */
    public RemoteProtocol(String serverIpAddress) {
        this.serverIpAddress = serverIpAddress;
        connectionErrorHandler = null;
    }

    /**
     * Ustawia obiekt obsługujący wyjątek.
     */
    public void setConnectionErrorHandler(ConnectionErrorHandler connectionErrorHandler) {
        this.connectionErrorHandler = connectionErrorHandler;
    }

    /**
     * Nawiązuje połączenie z serwerem oraz powiadamia o odebranych zdarzeniach.
     */
    @Override
    public void run() {
        try {
            socket = new Socket(serverIpAddress, MyServer.PORT_NUMBER);
            out = new ObjectOutputStream(socket.getOutputStream());
            in = new ObjectInputStream(socket.getInputStream());
        } catch (IOException e) {
            if (connectionErrorHandler != null) {
                connectionErrorHandler.uncaughtException(e);
            }
        }

        while (true) {
            try {
                ControllerEvent event = (ControllerEvent) in.readObject();
                notifyOfEvent(event);
            } catch (IOException | ClassNotFoundException e) {
                if (connectionErrorHandler != null) {
                    connectionErrorHandler.uncaughtException(e);
                }
                break;
            }
        }
    }

    /**
     * Wysyła zdarzenie klienta poprzez socket.
     */
    @Override
    public void sendEvent(ClientEvent event) {
        try {
            // Wstrzyknij informację o graczu.
            event.setPlayerType(PlayerType.REMOTE_PLAYER);
            out.writeObject(event);
            out.flush();
        } catch (IOException e) {
        }
    }

    @Override
    protected void finalize() throws Throwable {
        try {
            out.close();
            in.close();
            socket.close();
        } catch (IOException e) {
        } finally {
            super.finalize();
        }
    }
}
