package ships.controller;

import ships.events.ControllerEvent;

/**
 * Interfejs serwera.
 * Należy zaimplementować sposób wysyłania zdarzeń do graczy.
 */
public interface Server {

    /**
     * Wysyła zdarzenie do lokalnego gracza.
     */
    void sendEventToLocalPlayer(ControllerEvent event);

    /**
     * Wysyła zdarzenie do zdalnego gracza.
     */
    void sendEventToRemotePlayer(ControllerEvent event);
}
