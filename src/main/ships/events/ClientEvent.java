package ships.events;

import java.io.Serializable;

/**
 * Zdarzenie wychodzące od klienta.
 */
public class ClientEvent implements Serializable {

    protected PlayerType playerType;

    public ClientEvent() {
        playerType = null;
    }

    /**
     * Ustawia typ gracza.
     * Powinna być jedynie używana podczas przesyłania zdarzenia w celu
     * identyfikacji gracza.
     */
    public void setPlayerType(PlayerType playerType) {
        this.playerType = playerType;
    }

    /**
     * Zwraca typ gracza
     */
    public PlayerType getPlayerType() {
        return playerType;
    }
}
