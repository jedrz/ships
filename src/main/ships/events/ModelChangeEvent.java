package ships.events;

import ships.model.ModelChange;

/**
 * Zdarzenie zawierające zmianę modelu dla widoku.
 */
public class ModelChangeEvent extends ControllerEvent {

    private ModelChange modelChange;

    /**
     * Tworzy zdarzenie ze zmianę modelu..
     */
    public ModelChangeEvent(ModelChange modelChange) {
        this.modelChange = modelChange;
    }

    /**
     * Zwraca zmianę modelu.
     */
    public ModelChange getModelChange() {
        return modelChange;
    }
}
