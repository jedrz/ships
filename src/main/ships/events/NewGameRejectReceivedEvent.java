package ships.events;

/**
 * Zdarzenie oznaczające, że gracz nie zgadza się na nową grę.
 */
public class NewGameRejectReceivedEvent extends ControllerEvent {
}
