package ships.events;

/**
 * Zdarzenie oznaczające, że drugi gracz chce zacząć grę od nowa.
 */
public class NewGameRequestReceivedEvent extends ControllerEvent {
}
