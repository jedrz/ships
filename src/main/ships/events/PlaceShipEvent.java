package ships.events;

import ships.model.Coordinates;
import ships.model.Orientation;
import ships.model.ShipType;

/**
 * Zdarzenie oznaczające chęć umieszczenia statku.
 */
public class PlaceShipEvent extends ClientEvent {

    protected Coordinates coordinates;
    protected ShipType shipType;
    protected Orientation orientation;

    /**
     * @param shipType rodzaj statku
     * @param orientation orientacja
     * @param coordinates współrzędne początku
     */
    public PlaceShipEvent(ShipType shipType,
                          Orientation orientation,
                          Coordinates coordinates) {
        this.shipType = shipType;
        this.orientation = orientation;
        this.coordinates = coordinates;
    }

    /**
     * Zwraca rodzaj statku.
     */
    public ShipType getShipType() {
        return shipType;
    }

    /**
     * Zwraca orientację statku.
     */
    public Orientation getOrientation() {
        return orientation;
    }

    /**
     * Zwraca współrzędne początka statku.
     */
    public Coordinates getCoordinates() {
        return coordinates;
    }
}
