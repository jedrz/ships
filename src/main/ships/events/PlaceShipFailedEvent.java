package ships.events;

import ships.model.ShipType;

/**
 * Zdarzenie oznaczające, że próba ustawienia statku zakończyła się
 * niepowodzeniem.
 */
public class PlaceShipFailedEvent extends ControllerEvent {

    protected ShipType shipType;

    /**
     * Tworzy zdarzenie, które dotyczy konkretnego typu statku.
     */
    public PlaceShipFailedEvent(ShipType shipType) {
        this.shipType = shipType;
    }

    public ShipType getShipType() {
        return shipType;
    }
}

