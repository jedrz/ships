package ships.events;

import ships.model.ShipType;

/**
 * Zdarzenie oznaczające, że próba ustawienia statku zakończyła się
 * sukcesem.
 */
public class PlaceShipSuccessfulEvent extends ControllerEvent {

    protected ShipType shipType;

    /**
     * Tworzy zdarzenie, które dotyczy konkretnego typu statku.
     */
    public PlaceShipSuccessfulEvent(ShipType shipType) {
        this.shipType = shipType;
    }

    public ShipType getShipType() {
        return shipType;
    }
}
