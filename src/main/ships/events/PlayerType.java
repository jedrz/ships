package ships.events;

/**
 * Opisuje typ gracza.
 * Typ wyliczeniowy potrzebny do identyfikacji pochodzenia zdarzenia.
 * @see ClientEvent
 */
public enum PlayerType {
    LOCAL_PLAYER, REMOTE_PLAYER
}
