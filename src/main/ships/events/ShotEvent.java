package ships.events;

import ships.model.Coordinates;

/**
 * Zdarzenie strzału.
 */
public class ShotEvent extends ClientEvent {

    protected Coordinates shotCoordinates;

    /**
     * Tworzy zdarzenie o podanych współrzędnych strzału.
     */
    public ShotEvent(Coordinates shotCoordinates) {
        this.shotCoordinates = shotCoordinates;
    }

    /**
     * Zwraca współrzędne strzału.
     */
    public Coordinates getShotCoordinates() {
        return shotCoordinates;
    }
}
