package ships.model;

import java.io.Serializable;

/**
 * Klasa planszy przydatna do zarządzania planszami graczy.
 */
public class Board implements Serializable {

    /**
     * Rozmiar planszy.
     */
    public static final int SIZE = 10;

    protected final FieldType board[][];

    /**
     * Tworzy planszę z wartościami pól równymi {@link FieldType#FREE}.
     */
    public Board() {
        board = new FieldType[SIZE][SIZE];
        fillBoard(FieldType.FREE);
    }

    /**
     * Tworzy planszę z polami równymi podanej wartości.
     * @param fieldType pożądana wartość pól
     */
    public Board(FieldType fieldType) {
        board = new FieldType[SIZE][SIZE];
        fillBoard(fieldType);
    }

    private void fillBoard(FieldType fieldType) {
        for (int x = 0; x < SIZE; ++x) {
            for (int y = 0; y < SIZE; ++y) {
                board[x][y] = fieldType;
            }
        }
    }

    /**
     * Zwraca wartość pola dla podanych współrzędnych.
     */
    public FieldType getField(Coordinates coordinates) {
        return board[coordinates.getX()][coordinates.getY()];
    }

    /**
     * Zwraca wartość pola dla podanych współrzędnych.
     * @throws InvalidCoordinateException rzucany, gdy współrzędne są niepoprawne.
     */
    public FieldType getField(final int x, final int y)
            throws InvalidCoordinateException {
        Coordinates coordinates = new Coordinates(x, y);
        return getField(coordinates);
    }

    /**
     * Ustawia pole o podanych współrzędnych.
     */
    public void setField(Coordinates coordinates,
                         final FieldType fieldType) {
        board[coordinates.getX()][coordinates.getY()] = fieldType;
    }

    /**
     * Ustawia pole o podanych współrzędnych.
     * @throws InvalidCoordinateException rzucany, gdy współrzędne są niepoprawne
     */
    public void setField(final int x, final int y, final FieldType fieldType)
            throws InvalidCoordinateException {
        Coordinates coordinates = new Coordinates(x, y);
        setField(coordinates, fieldType);
    }
}
