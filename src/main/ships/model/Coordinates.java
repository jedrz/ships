package ships.model;

import java.io.Serializable;

/**
 * Współrzędne pola na planszy.
 */
public class Coordinates implements Serializable {

    protected final int x, y;

    /**
     * @param x współrzędna x
     * @param y współrzędna y
     * @throws InvalidCoordinateException wyjątek rzucany, gdy któraś ze
     * współrzędnych jest większa bądź równa rozmiarowi planszy
     * ({@link Board#SIZE}) lub mniejsza od 0
     */
    public Coordinates(int x, int y) throws InvalidCoordinateException {
        if (x < 0 || x >= Board.SIZE|| y < 0 || y >= Board.SIZE) {
            throw new InvalidCoordinateException();
        } else {
            this.x = x;
            this.y = y;
        }
    }

    /**
     * Zwraca współrzędną x
     */
    public int getX() {
        return x;
    }

    /**
     * Zwraca współrzędną y
     */
    public int getY() {
        return y;
    }
}
