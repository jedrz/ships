package ships.model;

/**
 * Opisuje możliwe stany pola na planszy.
 */
public enum FieldType {
    FREE, HIT, MISS, UNREACHABLE
}
