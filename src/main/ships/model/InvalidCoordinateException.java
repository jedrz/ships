package ships.model;

/**
 * Wyjątek rzucany, gdy współrzędne nie są poprawne.
 */
public class InvalidCoordinateException extends Exception {
}
