package ships.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Observable;
import java.util.Set;
import java.util.TreeSet;

public class Model extends Observable {

    protected Board localPlayerShots, remotePlayerShots;
    protected List<Ship> localPlayerShips, remotePlayerShips;
    protected Set<ShipType> localPlayerShipsToPlace, remotePlayerShipsToPlace;
    protected ModelChange localPlayerModelChange, remotePlayerModelChange;

    /**
     * Tworzy obiekt modelu.
     * Tworzy planszę oddanych strzałów, listę statków, zbiór pozostałych
     * do rozstawienia statków oraz puste zmiany modelu dla graczy.
     */
    public Model() {
        reset();
    }

    /**
     * Przywraca początkowy stan modelu.
     */
    public void reset() {
        localPlayerShots = new Board();
        localPlayerShips = new ArrayList<>();
        localPlayerShipsToPlace = generateShipsToPlace();
        localPlayerModelChange = new ModelChange();
        remotePlayerShots = new Board();
        remotePlayerShips = new ArrayList<>();
        remotePlayerShipsToPlace = generateShipsToPlace();
        remotePlayerModelChange = new ModelChange();
    }

    /**
     * Obsługuje strzał oddany przez lokalnego gracza.
     * @param shotCoordinates współrzędne strzału
     * @return <code>true</code> jeśli był celny
     */
    public boolean handleLocalPlayerShot(Coordinates shotCoordinates) {
        // Na pewno zmienią się plansze obu graczy, ale być może zostanie
        // zniszczony też statek.
        registerLocalPlayerChangeType(
                ModelChange.ChangeType.OPPONENT_BOARD_CHANGE);
        registerRemotePlayerChangeType(
                ModelChange.ChangeType.PLAYER_BOARD_CHANGE);
        for (Ship opponentShip : remotePlayerShips) {
            if (opponentShip.shoot(shotCoordinates)) {
                localPlayerShots.setField(shotCoordinates, FieldType.HIT);
                registerLocalPlayerShot(shotCoordinates, FieldType.HIT);
                // Oznacz pola, w których na pewno nie będzie innego statku,
                // jeśli ten został już zniszczony.
                if (opponentShip.isDestroyed()) {
                    markUnreachableFields(localPlayerShots, opponentShip);
                    registerRemotePlayerDestroyedShip(
                            opponentShip.getShipType(),
                            opponentShip.getOrientation(),
                            opponentShip.getCoordinates());
                    registerLocalPlayerChangeType(
                            ModelChange.ChangeType.OPPONENT_SHIP_DESTROYED);
                    registerRemotePlayerChangeType(
                            ModelChange.ChangeType.PLAYER_SHIP_DESTROYED);
                }
                notifyOfModelChange();
                return true;
            }
        }
        localPlayerShots.setField(shotCoordinates, FieldType.MISS);
        registerLocalPlayerShot(shotCoordinates, FieldType.MISS);
        notifyOfModelChange();
        return false;
    }

    /**
     * Obsługuje strzał oddany przez zdalnego gracza.
     * @param shotCoordinates współrzędne strzału
     * @return <code>true</code> jeśli był celny
     */
    public boolean handleRemotePlayerShot(Coordinates shotCoordinates) {
        // Na pewno zmienią się plansze obu graczy, ale być może zostanie
        // zniszczony też statek.
        registerRemotePlayerChangeType(
                ModelChange.ChangeType.OPPONENT_BOARD_CHANGE);
        registerLocalPlayerChangeType(
                ModelChange.ChangeType.PLAYER_BOARD_CHANGE);
        for (Ship opponentShip : localPlayerShips) {
            if (opponentShip.shoot(shotCoordinates)) {
                remotePlayerShots.setField(shotCoordinates, FieldType.HIT);
                registerRemotePlayerShot(shotCoordinates, FieldType.HIT);
                // Oznacz pola, w których na pewno nie będzie innego statku,
                // jeśli ten został już zniszczony.
                if (opponentShip.isDestroyed()) {
                    markUnreachableFields(remotePlayerShots, opponentShip);
                    registerLocalPlayerDestroyedShip(
                            opponentShip.getShipType(),
                            opponentShip.getOrientation(),
                            opponentShip.getCoordinates());
                    registerRemotePlayerChangeType(
                            ModelChange.ChangeType.OPPONENT_SHIP_DESTROYED);
                    registerLocalPlayerChangeType(
                            ModelChange.ChangeType.PLAYER_SHIP_DESTROYED);
                }
                notifyOfModelChange();
                return true;
            }
        }
        remotePlayerShots.setField(shotCoordinates, FieldType.MISS);
        registerRemotePlayerShot(shotCoordinates, FieldType.MISS);
        notifyOfModelChange();
        return false;
    }

    /**
     * Umieszcza statek lokalnego gracza na planszy.
     * @param shipType typ statku
     * @param orientation orientacja statku
     * @param coordinates współrzędne początku statku
     * @return <code>true</code> jeśli statek został ustawiony pomyślnie
     */
    public boolean placeLocalPlayerShip(ShipType shipType,
                                        Orientation orientation,
                                        Coordinates coordinates) {
        if (!localPlayerShipsToPlace.contains(shipType)) {
            return false;
        }
        if (isShipExceedingBoard(shipType, orientation, coordinates)) {
            return false;
        }
        Ship newShip = new Ship(shipType, orientation, coordinates);
        for (Ship ship : localPlayerShips) {
            if (newShip.collidesWith(ship)) {
                return false;
            }
        }
        localPlayerShips.add(newShip);
        localPlayerShipsToPlace.remove(shipType);
        registerLocalPlayerNewShip(shipType, orientation, coordinates);
        notifyOfModelChange();
        return true;
    }

    /**
     * Umieszcza statek zdalnego gracza na planszy.
     * @param shipType typ statku
     * @param orientation orientacja statku
     * @param coordinates współrzędne początku statku
     * @return <code>true</code> jeśli statek został ustawiony pomyślnie
     */
    public boolean placeRemotePlayerShip(ShipType shipType,
                                         Orientation orientation,
                                         Coordinates coordinates) {
        if (!remotePlayerShipsToPlace.contains(shipType)) {
            return false;
        }
        if (isShipExceedingBoard(shipType, orientation, coordinates)) {
            return false;
        }
        Ship newShip = new Ship(shipType, orientation, coordinates);
        for (Ship ship : remotePlayerShips) {
            if (newShip.collidesWith(ship)) {
                return false;
            }
        }
        remotePlayerShips.add(newShip);
        remotePlayerShipsToPlace.remove(shipType);
        registerRemotePlayerNewShip(shipType, orientation, coordinates);
        notifyOfModelChange();
        return true;
    }

    /**
     * Zwraca zmianę modelu dla graczy zawierającą niezniszczone statki przeciwnika.
     */
    public PlayersModelChange getNotDestroyedShips() {
        ModelChange localPlayerModelChange = new ModelChange();
        for (Ship ship : remotePlayerShips) {
            if (!ship.isDestroyed()) {
                ModelChange.ShipEntry shipEntry = new ModelChange.ShipEntry(
                        ship.getShipType(),
                        ship.getOrientation(),
                        ship.getCoordinates());
                localPlayerModelChange.addOpponentShip(shipEntry);
            }
        }

        ModelChange remotePlayerModelChange = new ModelChange();
        for (Ship ship : localPlayerShips) {
            if (!ship.isDestroyed()) {
                ModelChange.ShipEntry shipEntry = new ModelChange.ShipEntry(
                        ship.getShipType(),
                        ship.getOrientation(),
                        ship.getCoordinates());
                remotePlayerModelChange.addOpponentShip(shipEntry);
            }
        }

        return new PlayersModelChange(
                localPlayerModelChange, remotePlayerModelChange);
    }

    /**
     * Sprawdza czy lokalny gracz rozstawił wszystkie swoje statki.
     */
    public boolean areLocalPlayerShipsPlaced() {
        return localPlayerShipsToPlace.isEmpty();
    }

    /**
     * Sprawdza czy zdalny gracz rozstawił wszystkie swoje statki.
     */
    public boolean areRemotePlayerShipsPlaced() {
        return remotePlayerShipsToPlace.isEmpty();
    }

    /**
     * Sprawdza czy obaj gracze rozstawili wszystkie swoje statki.
     */
    public boolean arePlayersShipsPlaced() {
        return areLocalPlayerShipsPlaced() && areRemotePlayerShipsPlaced();
    }

    /**
     * Sprawdza czy lokalny gracz zwyciężył.
     */
    public boolean isLocalPlayerWinner() {
        for (Ship ship : remotePlayerShips) {
            if (!ship.isDestroyed()) {
                return false;
            }
        }
        return true;
    }

    /**
     * Sprawdza czy zdalny gracz zwyciężył.
     */
    public boolean isRemotePlayerWinner() {
        for (Ship ship : localPlayerShips) {
            if (!ship.isDestroyed()) {
                return false;
            }
        }
        return true;
    }

    /**
     * Generuje zbiór statków do rozmieszczenia przez gracza
     */
    protected Set<ShipType> generateShipsToPlace() {
        Set<ShipType> shipsSet = new TreeSet<>();
        shipsSet.addAll(Arrays.asList(ShipType.values()));
        return shipsSet;
    }

    /**
     * Sprawdza czy statek wykracza poza planszę.
     */
    protected boolean isShipExceedingBoard(ShipType shipType,
                                         Orientation orientation,
                                         Coordinates coordinates) {
        if (orientation == Orientation.HORIZONTAL) {
            return coordinates.getX() + shipType.getLength() > Board.SIZE;
        } else {
            return coordinates.getY() + shipType.getLength() > Board.SIZE;
        }
    }

    /**
     * Oznacza pola otaczające statek jako nieosiągalne.
     */
    protected void markUnreachableFields(Board board, Ship ship) {
        for (ShipPart shipPart : ship.getShipParts()) {
            for (int deltaX = -1; deltaX <= 1; ++deltaX) {
                for (int deltaY = -1; deltaY <= 1; ++deltaY) {
                    Coordinates coordinates;
                    try {
                        coordinates = new Coordinates(
                                shipPart.getCoordinates().getX() + deltaX,
                                shipPart.getCoordinates().getY() + deltaY);
                    } catch (InvalidCoordinateException e) {
                        continue;
                    }
                    // Oznaczam tylko te pola, które są wolne. Oddane strzały
                    // pozostawiam.
                    if (board.getField(coordinates) == FieldType.FREE) {
                        board.setField(coordinates, FieldType.UNREACHABLE);
                        // Zarejestruj zmianę odpowiedniej planszy.
                        if (board == localPlayerShots) {
                            registerLocalPlayerShot(coordinates,
                                    FieldType.UNREACHABLE);
                        } else {
                            registerRemotePlayerShot(coordinates,
                                    FieldType.UNREACHABLE);
                        }
                    }
                }
            }
        }
    }

    /**
     * Rejestruje zmianę modelu po oddaniu strzału przez lokalnego gracza.
     * @param coordinates współrzędne strzału
     * @param fieldType typ pola
     */
    protected void registerLocalPlayerShot(Coordinates coordinates,
                                         FieldType fieldType) {
        ModelChange.FieldEntry fieldEntry = new ModelChange.FieldEntry(
                coordinates, fieldType);
        localPlayerModelChange.addOpponentBoardChange(fieldEntry);
        remotePlayerModelChange.addPlayerBoardChange(fieldEntry);
    }

    /**
     * Rejestruje zmianę modelu po oddaniu strzału przez zdalnego gracza.
     * @param coordinates współrzędne strzału
     * @param fieldType typ pola
     */
    protected void registerRemotePlayerShot(Coordinates coordinates,
                                          FieldType fieldType) {
        ModelChange.FieldEntry fieldEntry = new ModelChange.FieldEntry(
                coordinates, fieldType);
        remotePlayerModelChange.addOpponentBoardChange(fieldEntry);
        localPlayerModelChange.addPlayerBoardChange(fieldEntry);

    }

    /**
     * Rejestruje zmianę modelu podczas umieszczenia nowego statku przez
     * lokalnego gracza
     * @param shipType typ statku
     * @param orientation orientacja statku
     * @param coordinates współrzędne początka statku
     */
    protected void registerLocalPlayerNewShip(ShipType shipType,
                                            Orientation orientation,
                                            Coordinates coordinates) {
        ModelChange.ShipEntry shipEntry = new ModelChange.ShipEntry(
                shipType, orientation, coordinates);
        localPlayerModelChange.addPlayerShip(shipEntry);
    }

    /**
     * Rejestruje zmianę modelu podczas umieszczenia nowego statku przez
     * zdalnego gracza
     * @param shipType typ statku
     * @param orientation orientacja statku
     * @param coordinates współrzędne początka statku
     */
    protected void registerRemotePlayerNewShip(ShipType shipType,
                                             Orientation orientation,
                                             Coordinates coordinates) {
        ModelChange.ShipEntry shipEntry = new ModelChange.ShipEntry(
                shipType, orientation, coordinates);
        remotePlayerModelChange.addPlayerShip(shipEntry);
    }

    /**
     * Rejestruje zmianę modelu podczas zniszczenia statku lokalnego gracza.
     * @param shipType typ statku
     * @param orientation orientacja statku
     * @param coordinates współrzędne początka statku
     */
    protected void registerLocalPlayerDestroyedShip(ShipType shipType,
                                                  Orientation orientation,
                                                  Coordinates coordinates) {
        ModelChange.ShipEntry shipEntry = new ModelChange.ShipEntry(
                shipType, orientation, coordinates);
        remotePlayerModelChange.addOpponentShip(shipEntry);
        localPlayerModelChange.setPlayerShipChange(shipEntry);
    }

    /**
     * Rejestruje zmianę modelu podczas zniszczenia statku zdalnego gracza.
     * @param shipType typ statku
     * @param orientation orientacja statku
     * @param coordinates współrzędne początka statku
     */
    protected void registerRemotePlayerDestroyedShip(ShipType shipType,
                                                   Orientation orientation,
                                                   Coordinates coordinates) {
        ModelChange.ShipEntry shipEntry = new ModelChange.ShipEntry(
                shipType, orientation, coordinates);
        localPlayerModelChange.addOpponentShip(shipEntry);
        remotePlayerModelChange.setPlayerShipChange(shipEntry);
    }

    /**
     * Rejestruje rodzaj zmiany dla lokalnego gracza.
     */
    protected void registerLocalPlayerChangeType(
            ModelChange.ChangeType changeType) {
        localPlayerModelChange.setChangeType(changeType);
    }


    /**
     * Rejestruje rodzaj zmiany dla zdalnego gracza.
     */
    protected void registerRemotePlayerChangeType(
            ModelChange.ChangeType changeType) {
        remotePlayerModelChange.setChangeType(changeType);
    }

    /**
     * Powiadamia obserwatorów (kontroler) o zmianie modelu, która powinna
     * być przekazana graczom.
     */
    protected void notifyOfModelChange() {
        setChanged();
        PlayersModelChange playersModelChange = new PlayersModelChange(
                localPlayerModelChange, remotePlayerModelChange);
        notifyObservers(playersModelChange);
        localPlayerModelChange = new ModelChange();
        remotePlayerModelChange = new ModelChange();
    }
}
