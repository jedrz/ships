package ships.model;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

/**
 * Zmiana modelu dla widoku.
 */
public class ModelChange implements Serializable {

    protected final List<FieldEntry> playerBoardChangeList, opponentBoardChangeList;
    protected final List<ShipEntry> playerShipList, opponentShipList;
    protected ChangeType changeType;
    protected ShipEntry playerShipChange;

    /**
     * Tworzy pustą zmianę modelu.
     */
    public ModelChange() {
        playerBoardChangeList = new LinkedList<>();
        opponentBoardChangeList = new LinkedList<>();
        playerShipList = new LinkedList<>();
        opponentShipList = new LinkedList<>();
        changeType = null;
        playerShipChange = null;
    }

    /**
     * Dodaje zmianę planszy gracza.
     */
    public void addPlayerBoardChange(FieldEntry fieldEntry) {
        playerBoardChangeList.add(fieldEntry);
    }

    /**
     * Zwraca listę zmian planszy gracza.
     */
    public List<FieldEntry> getPlayerBoardChangeList() {
        return playerBoardChangeList;
    }

    /**
     * Dodaje zmianę planszy przeciwnika.
     */
    public void addOpponentBoardChange(FieldEntry fieldEntry) {
        opponentBoardChangeList.add(fieldEntry);
    }

    /**
     * Zwraca listę zmian planszy przeciwnika.
     */
    public List<FieldEntry> getOpponentBoardChangeList() {
        return opponentBoardChangeList;
    }

    /**
     * Dodaje statek gracza.
     */
    public void addPlayerShip(ShipEntry shipEntry) {
        playerShipList.add(shipEntry);
    }

    /**
     * Zwraca listę nowych statków gracza.
     */
    public List<ShipEntry> getPlayerShipList() {
        return playerShipList;
    }

    /**
     * Dodaje statek przeciwnika.
     */
    public void addOpponentShip(ShipEntry shipEntry) {
        opponentShipList.add(shipEntry);
    }

    /**
     * Zwraca listę nowych statków przeciwnika.
     */
    public List<ShipEntry> getOpponentShipList() {
        return opponentShipList;
    }

    /**
     * Ustawia rodzaj zmiany.
     */
    public void setChangeType(ChangeType changeType) {
        this.changeType = changeType;
    }

    /**
     * Zwraca rodzaj zmiany, który zawiera obiekt.
     */
    public ChangeType getChangeType() {
        return changeType;
    }

    /**
     * Zwraca zmianę w planszy gracza.
     * Na użytek wyświetlania listy zmian dla gracza.
     */
    public FieldEntry getPlayerBoardChange() {
        if (changeType == ChangeType.PLAYER_BOARD_CHANGE
                || changeType == ChangeType.PLAYER_SHIP_DESTROYED) {
            for (FieldEntry fieldEntry : playerBoardChangeList) {
                if (fieldEntry.getFieldType() == FieldType.HIT
                        || fieldEntry.getFieldType() == FieldType.MISS) {
                    return fieldEntry;
                }
            }
        }
        return null;
    }

    /**
     * Zwraca zmianę w planszy przeciwnika.
     * Na użytek wyświetlania listy zmian dla gracza.
     */
    public FieldEntry getOpponentBoardChange() {
        if (changeType == ChangeType.OPPONENT_BOARD_CHANGE
                || changeType == ChangeType.OPPONENT_SHIP_DESTROYED) {
            for (FieldEntry fieldEntry : opponentBoardChangeList) {
                if (fieldEntry.getFieldType() == FieldType.HIT
                        || fieldEntry.getFieldType() == FieldType.MISS) {
                    return fieldEntry;
                }
            }
        }
        return null;
    }

    /**
     * Zwraca zmianę statku gracza.
     * Na użytek wyświetlania listy zmian dla gracza.
     */
    public ShipEntry getPlayerShipChange() {
        return playerShipChange;
    }

    /**
     * Ustawia statek gracza, który uległ zmianie.
     * @param playerShipChange opis statku
     */
    public void setPlayerShipChange(ShipEntry playerShipChange) {
        this.playerShipChange = playerShipChange;
    }

    /**
     * Zwraca zmianę statku przeciwnika.
     * Na użytek wyświetlania listy zmian dla gracza.
     */
    public ShipEntry getOpponentShipChange() {
        if (changeType == ChangeType.OPPONENT_SHIP_DESTROYED) {
            return opponentShipList.get(0);
        }
        return null;
    }

    /**
     * Klasa reprezentuje pola na planszy.
     * Składa się z współrzędnych i typu pola.
     */
    static public class FieldEntry implements Serializable {

        protected final Coordinates coordinates;
        protected final FieldType fieldType;

        /**
         * Tworzy parę współrzędne-typ pola.
         * @param coordinates współrzędne pola
         * @param fieldType typ pola
         */
        public FieldEntry(Coordinates coordinates, FieldType fieldType) {
            this.coordinates = coordinates;
            this.fieldType = fieldType;
        }

        /**
         * Zwraca współrzędne.
         */
        public Coordinates getCoordinates() {
            return coordinates;
        }

        /**
         * Zwraca typ pola.
         */
        public FieldType getFieldType() {
            return fieldType;
        }
    }

    /**
     * Klasa reprezentuje statek na planszy.
     * Składa się z typu statku, orientację oraz współrzędnych początka.
     */
    static public class ShipEntry implements Serializable {

        protected final ShipType shipType;
        protected final Orientation orientation;
        protected final Coordinates coordinates;

        /**
         * Tworzy opis statku.
         * @param shipType typ statku
         * @param orientation orientacja statku
         * @param coordinates współrzędne początka
         */
        public ShipEntry(ShipType shipType,
                         Orientation orientation,
                         Coordinates coordinates) {
            this.shipType = shipType;
            this.orientation = orientation;
            this.coordinates = coordinates;
        }

        /**
         * Zwraca typ statku.
         */
        public ShipType getShipType() {
            return shipType;
        }

        /**
         * Zwraca orientację.
         */
        public Orientation getOrientation() {
            return orientation;
        }

        /**
         * Zwraca współrzędne początka.
         */
        public Coordinates getCoordinates() {
            return coordinates;
        }
    }

    /**
     * Informacja o tym czego dotyczy zmiana modelu.
     */
    static public enum ChangeType {
        PLAYER_SHIP_DESTROYED,
        OPPONENT_SHIP_DESTROYED,
        PLAYER_BOARD_CHANGE,
        OPPONENT_BOARD_CHANGE
    }
}
