package ships.model;

/**
 * Opisuje orientację statku - poziomy czy pionowy.
 */
public enum Orientation {
    HORIZONTAL, VERTICAL;

    /**
     * Zwraca przeciwną orientację do aktualnej.
     */
    public Orientation toggle() {
        if (this == HORIZONTAL) {
            return VERTICAL;
        } else {
            return HORIZONTAL;
        }
    }
}
