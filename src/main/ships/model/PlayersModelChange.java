package ships.model;

/**
 * Klasa zawiera zmianę modelu dla dwóch graczy.
 * Na użytek kontrolera.
 */
public class PlayersModelChange {

    protected final ModelChange localPlayerModelChange, remotePlayerModelChange;

    /**
     * Tworzy zmianę modelu dla dwóch graczy.
     * @param localPlayerModelChange zmiana modelu dla lokalnego gracza
     * @param remotePlayerModelChange zmiana modelu dla zdalnego gracza
     */
    public PlayersModelChange(ModelChange localPlayerModelChange,
                              ModelChange remotePlayerModelChange) {
        this.localPlayerModelChange = localPlayerModelChange;
        this.remotePlayerModelChange = remotePlayerModelChange;
    }

    /**
     * Zwraca zmianę modelu dla lokalnego gracza.
     */
    public ModelChange getLocalPlayerModelChange() {
        return localPlayerModelChange;
    }

    /**
     * Zwraca zmianę modelu dla zdalnego gracza.
     */
    public ModelChange getRemotePlayerModelChange() {
        return remotePlayerModelChange;
    }
}
