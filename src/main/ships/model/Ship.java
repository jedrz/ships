package ships.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Opisuje statek.
 */
public class Ship {

    protected final List<ShipPart> shipParts;
    protected final ShipType shipType;
    protected final Orientation orientation;
    protected final Coordinates coordinates;

    /**
     * Tworzy nowy statek.
     * @param shipType typ statku
     * @param orientation orientacja
     * @param start współrzędne początku
     */
    public Ship(ShipType shipType, Orientation orientation, Coordinates start) {
        shipParts = new ArrayList<>();
        this.shipType = shipType;
        this.orientation = orientation;
        this.coordinates = start;
        if (orientation == Orientation.HORIZONTAL) {
            for (int x = start.getX(); x < start.getX() + shipType.getLength(); ++x) {
                try {
                    shipParts.add(new ShipPart(new Coordinates(x, start.getY())));
                } catch (InvalidCoordinateException e) {
                }
            }
        } else {
            for (int y = start.getY(); y < start.getY() + shipType.getLength(); ++y) {
                try {
                    shipParts.add(new ShipPart(new Coordinates(start.getX(), y)));
                } catch (InvalidCoordinateException e) {
                }
            }
        }
    }

    /**
     * Sprawdza czy statek jest zniszczony.
     * Statek jest zniszczony, gdy wszystkie jego elementy zostały zniszczone.
     * @return <code>true</code> jeśli statek zniszczony, <code>false</code>
     * w przeciwnym wypadku
     */
    public boolean isDestroyed() {
        boolean destroyed = true;
        for (ShipPart shipPart : shipParts) {
            if (!shipPart.isDestroyed()) {
                destroyed = false;
                break;
            }
        }
        return destroyed;
    }

    /**
     * Sprawdza czy statek koliduje z podanym.
     * @param ship statek, z którym jest sprawdzana kolizja
     * @return <code>true</code> jeśli występuje kolizja, <code>false</code>
     * w przeciwnym wypadku
     * @see ShipPart#collidesWith(ShipPart)
     */
    public boolean collidesWith(Ship ship) {
        for (ShipPart shipPart : shipParts) {
            for (ShipPart otherShipPart : ship.shipParts) {
                if (shipPart.collidesWith(otherShipPart)) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Obsługuje strzał w statek o podanych współrzędnych
     * @param shotCoordinates współrzędne strzału
     * @return <code>true</code> jeśli jakiś element statku został trafiony
     */
    public boolean shoot(Coordinates shotCoordinates) {
        for (ShipPart shipPart : shipParts) {
            if (shipPart.shoot(shotCoordinates)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Zwraca listę elementów statku.
     */
    public List<ShipPart> getShipParts() {
        return shipParts;
    }

    /**
     * Zwraca typ statku.
     */
    public ShipType getShipType() {
        return shipType;
    }

    /**
     * Zwraca orientację statku.
     */
    public Orientation getOrientation() {
        return orientation;
    }

    /**
     * Zwraca współrzędne początku statku.
     */
    public Coordinates getCoordinates() {
        return coordinates;
    }
}