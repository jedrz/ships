package ships.model;

/**
 * Opisuje element statku.
 */
public class ShipPart {

    protected final Coordinates coordinates;
    protected boolean destroyed;

    /**
     * Tworzy element statku o podanych współrzędnych.
     */
    public ShipPart(Coordinates coordinates) {
        this.coordinates = coordinates;
        destroyed = false;
    }

    /**
     * Zwraca współrzędne elementu statku.
     */
    public Coordinates getCoordinates() {
        return coordinates;
    }

    /**
     * Sprawdza czy element statku jest zniszczony.
     */
    public boolean isDestroyed() {
        return destroyed;
    }

    /**
     * Obsługuje strzał o podanych współrzędnych w element statku.
     * @param shotCoordinates współrzędne strzału
     * @return <code>true</code> jeśli współrzędne strzału są takie same
     * jak elementu statku, w przeciwnym wypadku <code>false</code>
     */
    public boolean shoot(Coordinates shotCoordinates) {
        if (coordinates.getX() == shotCoordinates.getX()
                && coordinates.getY() == shotCoordinates.getY()) {
            destroyed = true;
            return true;
        }
        return false;
    }

    /**
     * Sprawdza czy element statku koliduje z podanym.
     * Aby wystąpiła kolizja różnica obu współrzędnych musi być równa 0 lub 1.
     * @return <code>true</code> jeśli występuje kolizja, <code>false</code>
     * w przeciwnym wypadku
     */
    public boolean collidesWith(ShipPart shipPart) {
        Coordinates otherCoordinates = shipPart.getCoordinates();
        return Math.abs(coordinates.getX() - otherCoordinates.getX()) <= 1
                && Math.abs(coordinates.getY() - otherCoordinates.getY()) <= 1;
    }
}