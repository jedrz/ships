package ships.model;

/**
 * Statki dostępne podczas rozgrywki.
 */
public enum ShipType {
    FOUR_MASTED(4),
    THREE_MASTED_1(3),
    THREE_MASTED_2(3),
    TWO_MASTED_1(2),
    TWO_MASTED_2(2),
    TWO_MASTED_3(2),
    ONE_MASTED_1(1),
    ONE_MASTED_2(1),
    ONE_MASTED_3(1),
    ONE_MASTED_4(1);

    private int length;

    private ShipType(int length) {
        this.length = length;
    }

    /**
     * @return długość statku
     */
    public int getLength() {
        return length;
    }
}
