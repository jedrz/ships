package ships.view;

import java.awt.GridLayout;
import java.util.List;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import ships.model.Board;
import ships.model.Coordinates;
import ships.model.FieldType;
import ships.model.InvalidCoordinateException;
import ships.model.ModelChange;
import ships.model.Orientation;
import ships.model.ShipType;

/**
 * Umożliwia wyświetlenie widoku planszy.
 */
public class BoardView {

    protected final JPanel panel;
    protected final Field fieldTable[][];

    /**
     * Tworzy panel zawierający planszę o wymiarach 10x10 oraz oznaczenia współrzędnych.
     * Dodatkowo tworzona jest tablica obiektów {@link Field}.
     */
    public BoardView() {
        panel = new JPanel();
        panel.setLayout(new GridLayout(Board.SIZE + 1, Board.SIZE + 1, 3, 3));
        fieldTable = new Field[Board.SIZE][Board.SIZE];
        JLabel yxLabel = new JLabel("y\\x");
        yxLabel.setHorizontalAlignment(SwingConstants.RIGHT);
        panel.add(yxLabel);
        for (int x = 0; x < Board.SIZE; ++x) {
            JLabel xLabel = new JLabel(Integer.toString(x));
            xLabel.setHorizontalAlignment(SwingConstants.CENTER);
            panel.add(xLabel);
        }
        for (int y = 0; y < Board.SIZE; ++y) {
            for (int x = 0; x < Board.SIZE; ++x) {
                if (x == 0) {
                    JLabel yLabel = new JLabel(Integer.toString(y));
                    yLabel.setHorizontalAlignment(SwingConstants.CENTER);
                    panel.add(yLabel);
                }
                Coordinates coordinates = null;
                try {
                    coordinates = new Coordinates(x, y);
                } catch (InvalidCoordinateException e) {
                    e.printStackTrace();
                }
                fieldTable[x][y] = new Field(coordinates);
                panel.add(fieldTable[x][y].getPanel());
            }
        }
    }

    /**
     * Zwraca panel planszy.
     */
    public JPanel getPanel() {
        return panel;
    }

    /**
     * Uaktualnia widok planszy.
     * @param boardChangeList lista zmian pól na planszy
     * @param shipList lista nowych statków
     */
    public void update(List<ModelChange.FieldEntry> boardChangeList,
                       List<ModelChange.ShipEntry> shipList) {
        for (ModelChange.FieldEntry fieldEntry : boardChangeList) {
            FieldType fieldType = fieldEntry.getFieldType();
            Coordinates coordinates = fieldEntry.getCoordinates();
            Field field = fieldTable[coordinates.getX()][coordinates.getY()];
            field.setShot(fieldType);
            field.redraw();
        }
        for (ModelChange.ShipEntry shipEntry : shipList) {
            ShipType shipType = shipEntry.getShipType();
            Orientation orientation = shipEntry.getOrientation();
            Coordinates coordinates = shipEntry.getCoordinates();
            if (orientation == Orientation.HORIZONTAL) {
                for (int x = coordinates.getX();
                     x < coordinates.getX() + shipType.getLength();
                     ++x) {
                    Field field = fieldTable[x][coordinates.getY()];
                    field.setShip(true);
                    field.redraw();
                }
            } else {
                for (int y = coordinates.getY();
                     y < coordinates.getY() + shipType.getLength();
                     ++y) {
                    Field field = fieldTable[coordinates.getX()][y];
                    field.setShip(true);
                    field.redraw();
                }
            }
        }
    }

    /**
     * Przywraca początkowy wygląd planszy.
     */
    public void reset() {
        for (int y = 0; y < Board.SIZE; ++y) {
            for (int x = 0; x < Board.SIZE; ++x) {
                Field field = fieldTable[x][y];
                field.setShot(FieldType.FREE);
                field.setShip(false);
                field.setPreviewShip(false);
                field.redraw();
            }
        }
    }
}

