package ships.view;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import javax.swing.JPanel;
import ships.model.Coordinates;
import ships.model.FieldType;

/**
 * Klasa reprezentująca pojedyncze pole planszy.
 */
public class Field {

    protected final FieldPanel fieldPanel;
    protected final Coordinates coordinates;
    protected FieldType shot;
    protected boolean ship, previewShip;

    protected static final Color COLOR_FREE = Color.LIGHT_GRAY,
            COLOR_MISS = Color.RED,
            COLOR_HIT = Color.GREEN,
            COLOR_UNREACHABLE = Color.DARK_GRAY,
            COLOR_SHIP = Color.BLACK,
            COLOR_PREVIEW_SHIP = Color.GRAY;

    /**
     * Tworzy pole powiązane podanymi współrzędnymi.
     * Domyślnie nie ma na nim statku, oddanego strzału oraz podglądu.
     */
    public Field(Coordinates coordinates) {
        this.coordinates = coordinates;
        this.shot = FieldType.FREE;
        this.ship = false;
        this.previewShip = false;
        fieldPanel = new FieldPanel();
    }

    /**
     * Zwraca współrzędne powiązane z polem.
     */
    public Coordinates getCoordinates() {
        return coordinates;
    }

    /**
     * Ustawia atrybut strzału na podaną wartość.
     */
    public void setShot(FieldType shot) {
        this.shot = shot;
    }

    /**
     * Zwraca atrybut strzału.
     */
    public FieldType getShot() {
        return shot;
    }

    /**
     * Ustawia czy statek jest skojarzony z polem.
     * @param ship <code>true</code> jeśli statek stoi na polu,
     *             <code>false</code> w przeciwnym wypadku
     */
    public void setShip(boolean ship) {
        this.ship = ship;
    }

    /**
     * Ustawia podgląd.
     * @param previewShip <code>true</code> jeśli należy rysować podgląd
     *                    statku, <code>false</code> w przeciwnym wypadku
     */
    public void setPreviewShip(boolean previewShip) {
        this.previewShip = previewShip;
    }

    /**
     * Zwraca panel.
     */
    public JPanel getPanel() {
        return fieldPanel;
    }

    /**
     * Przerysowuje pole.
     * Gdy na pole jest statek, tło jest czarne, gdy nie ma szare.
     * Podgląd - trochę mniejszy niż wymiar pola ciemnoszary kwadrat.
     * Celny strzał - zielone kółko, pudło - czerwone kółko.
     * Pole nieosiągalne - X.
     */
    public void redraw() {
        fieldPanel.repaint();
    }

    /**
     * Prywatna klasa dziedzicząca z <code>JPanel</code>.
     * Dziedziczenie konieczne, aby zaimplementować własne rysowanie.
     */
    protected class FieldPanel extends JPanel {

        @Override
        public void paintComponent(Graphics graphics) {
            super.paintComponent(graphics);

            Graphics2D g = (Graphics2D) graphics;

            // Narysuj oznaczenie statku lub jego braku.
            int fieldSize = getHeight();
            if (ship) {
                g.setColor(COLOR_SHIP);
                g.fillRect(0, 0, fieldSize, fieldSize);
            } else {
                g.setColor(COLOR_FREE);
                g.fillRect(0, 0, fieldSize, fieldSize);
            }

            // Narysuj podgląd.
            int previewSize = (int) (fieldSize * 0.8);
            int previewStartCoordinate = (fieldSize - previewSize) / 2;
            if (previewShip) {
                g.setColor(COLOR_PREVIEW_SHIP);
                g.fillRect(previewStartCoordinate, previewStartCoordinate,
                        previewSize, previewSize);
            }

            // Narysuj atrybut strzału.
            int shotRadius = (int) (fieldSize * 0.7);
            int startShotCoordinate = (fieldSize - shotRadius) / 2;
            switch (shot) {
                case HIT:
                    g.setColor(COLOR_HIT);
                    g.fillOval(startShotCoordinate, startShotCoordinate,
                            shotRadius, shotRadius);
                    break;
                case MISS:
                    g.setColor(COLOR_MISS);
                    g.fillOval(startShotCoordinate, startShotCoordinate,
                            shotRadius, shotRadius);
                    break;
                case UNREACHABLE:
                    g.setColor(COLOR_UNREACHABLE);
                    g.setStroke(new BasicStroke(5));
                    g.drawLine(0, fieldSize, fieldSize, 0);
                    g.drawLine(0, 0, fieldSize, fieldSize);
                    break;
            }
        }
    }
}
