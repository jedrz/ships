package ships.view;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JToolBar;
import javax.swing.WindowConstants;
import javax.swing.border.TitledBorder;
import ships.controller.Protocol;

/**
 * Klasa tworząca ramkę.
 */
public class GameWindow {

    protected final JFrame frame;
    protected final Menu menu;
    protected final JPanel centerContainer;
    protected final PlayerBoardView playerBoardView;
    protected final OpponentBoardView opponentBoardView;
    protected final ShipTypesPanel shipTypesPanel;
    protected final HistoryList historyList;
    protected final JLabel statusLabel;
    protected final RemainingShipsLabel remainingShipsLabel;

    /**
     * Tworzy ramkę wraz z panelami gry.
     * @param view obiekt widoku wymagany do ustawiania statków
     * @param protocol obiekt protokołu do wysłania prośby o nową grę
     */
    public GameWindow(final View view, final Protocol protocol) {
        frame = new JFrame("statki");
        frame.setResizable(false);
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.setLayout(new BoxLayout(frame.getContentPane(),
                BoxLayout.PAGE_AXIS));
        menu = new Menu(protocol);
        frame.setJMenuBar(menu.getMenuBar());

        JToolBar toolBar = new JToolBar();
        toolBar.setFloatable(false);
        statusLabel = new JLabel("Oczekiwanie na połączenie z drugim graczem");
        toolBar.add(statusLabel);
        toolBar.add(Box.createHorizontalGlue());
        frame.add(toolBar);

        centerContainer = new JPanel();
        centerContainer.setLayout(new FlowLayout());

        shipTypesPanel = new ShipTypesPanel(view);
        centerContainer.add(shipTypesPanel.getPanel());

        playerBoardView = new PlayerBoardView();
        playerBoardView.getPanel().setPreferredSize(new Dimension(400, 400));
        TitledBorder playerBoardBorder = BorderFactory.createTitledBorder(
                BorderFactory.createLineBorder(Color.BLACK), "Twoja plansza");
        playerBoardBorder.setTitleJustification(TitledBorder.CENTER);
        playerBoardView.getPanel().setBorder(playerBoardBorder);
        centerContainer.add(playerBoardView.getPanel());

        centerContainer.add(Box.createHorizontalStrut(10));

        opponentBoardView = new OpponentBoardView();
        opponentBoardView.getPanel().setPreferredSize(new Dimension(400, 400));
        TitledBorder opponentBoardBorder = BorderFactory.createTitledBorder(
                BorderFactory.createLineBorder(Color.BLACK), "Plansza przeciwnika");
        opponentBoardBorder.setTitleJustification(TitledBorder.CENTER);
        opponentBoardView.getPanel().setBorder(opponentBoardBorder);
        centerContainer.add(opponentBoardView.getPanel());

        historyList = new HistoryList();
        JScrollPane listScroller = new JScrollPane(historyList.getList());
        listScroller.setPreferredSize(new Dimension(350, 400));
        centerContainer.add(listScroller);

        frame.add(centerContainer);

        JToolBar bottomToolBar = new JToolBar();
        bottomToolBar.setFloatable(false);
        remainingShipsLabel = new RemainingShipsLabel();
        bottomToolBar.add(remainingShipsLabel.getLabel());
        bottomToolBar.add(Box.createHorizontalGlue());
        frame.add(bottomToolBar);
    }

    /**
     * Zwraca ramkę.
     */
    public JFrame getFrame() {
        return frame;
    }

    /**
     * Zwraca widok planszy gracza.
     */
    public PlayerBoardView getPlayerBoardView() {
        return playerBoardView;
    }

    /**
     * Zwraca widok planszy przeciwnika.
     */
    public OpponentBoardView getOpponentBoardView() {
        return opponentBoardView;
    }

    /**
     * Zwraca obiekt panelu ze statkami do wyboru.
     */
    public ShipTypesPanel getShipTypesPanel() {
        return shipTypesPanel;
    }

    /**
     * Zwraca obiekt listy zmian zachodzących w czasie rozgrywki.
     */
    public HistoryList getHistoryList() {
        return historyList;
    }

    /**
     * Zwraca obiekt do wyświetlania pozostałych statków.
     */
    public RemainingShipsLabel getRemainingShipsLabel() {
        return remainingShipsLabel;
    }

    /**
     * Zwraca menu.
     */
    public Menu getMenu() {
        return menu;
    }

    /**
     * Wyświetla ramkę.
     */
    public void showFrame() {
        frame.pack();
        frame.setVisible(true);
    }

    /**
     * Ustawia status.
     * @param statusText nowy tekst statusu
     */
    public void setStatus(String statusText) {
        statusLabel.setText(statusText);
    }
}
