package ships.view;

import javax.swing.DefaultListModel;
import javax.swing.JList;
import ships.model.FieldType;
import ships.model.ModelChange;

/**
 * Klasa do rejestracji zdarzeń, które zaszły podczas rozgrywki.
 */
public class HistoryList {

    protected final JList<String> list;
    protected final DefaultListModel<String> listModel;

    /**
     * Tworzy listę oraz model dla listy.
     */
    public HistoryList() {
        listModel = new DefaultListModel<>();
        list = new JList<>(listModel);
        list.setEnabled(false);
    }

    /**
     * Zwraca listę
     */
    public JList getList() {
        return list;
    }

    /**
     * Dodaje informację o zmianie pola na planszy gracza.
     * @param fieldEntry opis zmiany
     */
    public void addPlayerBoardChange(ModelChange.FieldEntry fieldEntry) {
        int x = fieldEntry.getCoordinates().getX();
        int y = fieldEntry.getCoordinates().getY();
        String desc;
        if (fieldEntry.getFieldType() == FieldType.HIT) {
            desc = "Celny strzał przeciwnika";
        } else {
            desc = "Pudło przeciwnika";
        }
        desc += " (" + x + ", " + y + ")";
        addElement(desc);
    }

    /**
     * Dodaje informację o zmianie pola na planszy przeciwnika.
     * @param fieldEntry opis zmiany
     */
    public void addOpponentBoardChange(ModelChange.FieldEntry fieldEntry) {
        int x = fieldEntry.getCoordinates().getX();
        int y = fieldEntry.getCoordinates().getY();
        String desc;
        if (fieldEntry.getFieldType() == FieldType.HIT) {
            desc = "Trafiony";
        } else {
            desc = "Pudło";
        }
        desc += " (" + x + ", " + y + ")!";
        addElement(desc);
    }

    /**
     * Dodaje informację o zniszczeniu statku gracza.
     * @param shipEntry opis statku
     */
    public void addPlayerShipDestroyed(ModelChange.ShipEntry shipEntry) {
        String shipName = View.getNameForShipType(shipEntry.getShipType());
        String desc = "Twój " + shipName + " zniszczony";
        addElement(desc);
    }

    /**
     * Dodaje informację o zniszczeniu statku przeciwnika.
     * @param shipEntry opis statku
     */
    public void addOpponentShipDestroyed(ModelChange.ShipEntry shipEntry) {
        String shipName = View.getNameForShipType(shipEntry.getShipType());
        String desc = "Statek przeciwnika " + shipName + " zniszczony!";
        addElement(desc);
    }

    /**
     * Dodaje element oraz upewnia się, że jest widoczny
     */
    protected void addElement(String element) {
        listModel.addElement(element);
        int lastIndex = listModel.getSize() - 1;
        list.ensureIndexIsVisible(lastIndex);
    }

    /**
     * Czyści listę.
     */
    public void reset() {
        listModel.clear();
    }
}
