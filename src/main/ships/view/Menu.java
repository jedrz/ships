package ships.view;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import ships.controller.Protocol;
import ships.events.NewGameRequestEvent;

/**
 * Klasa opakowująca <code>JMenu</code>.
 * Menu zawiera przyciski do prośby o rozpoczęcie nowej gry oraz
 * wyjścia z gry.
 */
public class Menu {

    protected final JMenuBar menuBar;

    /**
     * Buduje menu.
     */
    public Menu(final Protocol protocol) {
        menuBar = new JMenuBar();
        JMenu menu = new JMenu("Plik");
        menuBar.add(menu);

        JMenuItem newGameItem = new JMenuItem("Nowa gra");
        newGameItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                protocol.sendEvent(new NewGameRequestEvent());
            }
        });
        menu.add(newGameItem);

        JMenuItem exitItem = new JMenuItem("Wyjdź");
        exitItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.exit(0);
            }
        });
        menu.add(exitItem);
    }

    /**
     * Zwraca menu
     */
    public JMenuBar getMenuBar() {
        return menuBar;
    }
}
