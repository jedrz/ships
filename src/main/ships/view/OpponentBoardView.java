package ships.view;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import javax.swing.JPanel;
import ships.controller.Protocol;
import ships.events.ShotEvent;
import ships.model.Board;
import ships.model.Coordinates;
import ships.model.FieldType;

/**
 * Klasa oprócz wyświetla planszy, umożliwia graczowi strzały.
 */
public class OpponentBoardView extends BoardView {

    protected boolean playerTurn;

    /**
     * Na początku uniemożliwia strzelanie.
     * @see BoardView
     */
    public OpponentBoardView() {
        playerTurn = false;
    }

    /**
     * Usuwa słuchaczy pól
     * @see BoardView#reset
     */
    @Override
    public void reset() {
        super.reset();
        removeFieldListeners();
    }

    /**
     * Dodaje słuchaczy do każdego z pól planszy.
     * @param protocol obiekt protokołu wymagany do wysłania
     *                 zdarzenia strzału
     */
    public void addFieldListeners(final Protocol protocol) {
        for (int y = 0; y < Board.SIZE; ++y) {
            for (int x = 0; x < Board.SIZE; ++x) {
                Field field = fieldTable[x][y];
                Coordinates coordinates = field.getCoordinates();
                JPanel fieldPanel = field.getPanel();
                fieldPanel.addMouseListener(
                        new FieldListener(coordinates, protocol));
            }
        }
    }

    /**
     * Usuwa słuchaczy pól.
     */
    public void removeFieldListeners() {
        for (int y = 0; y < Board.SIZE; ++y) {
            for (int x = 0; x < Board.SIZE; ++x) {
                JPanel panel = fieldTable[x][y].getPanel();
                for (MouseListener mouseListener : panel.getMouseListeners()) {
                    panel.removeMouseListener(mouseListener);
                }
            }
        }
    }

    /**
     * Ustawia turę.
     * @param playerTurn <code>true</code> jeśli teraz jest tura gracza,
     *                   <code>false</code> tura przeciwnika
     */
    public void setPlayerTurn(boolean playerTurn) {
        this.playerTurn = playerTurn;
    }

    /**
     * Słuchacz pól planszy przeciwnika.
     * Gdy nastąpi kliknięcie na konkretne pole wysyłane jest zdarzenie strzału
     */
    protected class FieldListener extends MouseAdapter {

        protected final Coordinates coordinates;
        protected final Protocol protocol;

        /**
         * Tworzy obiekt słuchacza powiązany z podanymi współrzędnymi.
         * @param coordinates współrzędne nasłuchiwanego pola
         * @param protocol protokół do wysłania zdarzenia strzału
         */
        public FieldListener(Coordinates coordinates,
                             Protocol protocol) {
            this.coordinates = coordinates;
            this.protocol = protocol;
        }

        /**
         * Obsługuje kliknięcie - wysyła zdarzenie strzału.
         */
        @Override
        public void mouseClicked(MouseEvent e) {
            if (playerTurn) {
                Field field = fieldTable[coordinates.getX()][coordinates.getY()];
                if (field.getShot() == FieldType.FREE) {
                    protocol.sendEvent(new ShotEvent(coordinates));
                }
            }
        }
    }
}