package ships.view;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import javax.swing.JPanel;
import ships.controller.Protocol;
import ships.events.PlaceShipEvent;
import ships.model.Board;
import ships.model.Coordinates;
import ships.model.Orientation;
import ships.model.ShipType;

/**
 * Klasa oprócz wyświetlania planszy, umożliwia obsługę ustawiania statków
 * oraz wyświetlania podglądu.
 */
public class PlayerBoardView extends BoardView {

    protected ShipType shipType;
    protected Orientation orientation;

    public PlayerBoardView() {
        shipType = null;
    }

    /**
     * Umożliwia wyświetlenie podglądu poziomo podanego statku po najechaniu myszką.
     * @param shipType typ statku
     */
    public void placeShip(ShipType shipType) {
        this.shipType = shipType;
        orientation = Orientation.HORIZONTAL;
    }

    /**
     * Dodaje słuchacza do każdego pola.
     * @param protocol obiekt protokołu potrzebny do wysłania prośby
     *                 o ustawienie statku
     */
    public void addFieldListeners(final Protocol protocol) {
        for (int y = 0; y < Board.SIZE; ++y) {
            for (int x = 0; x < Board.SIZE; ++x) {
                Field field = fieldTable[x][y];
                Coordinates coordinates = field.getCoordinates();
                JPanel fieldPanel = field.getPanel();
                fieldPanel.addMouseListener(
                        new FieldListener(coordinates, protocol));
            }
        }
    }

    /**
     * Usuwa słuchaczy pól.
     */
    public void removeFieldListeners() {
        for (int y = 0; y < Board.SIZE; ++y) {
            for (int x = 0; x < Board.SIZE; ++x) {
                JPanel panel = fieldTable[x][y].getPanel();
                for (MouseListener mouseListener : panel.getMouseListeners()) {
                    panel.removeMouseListener(mouseListener);
                }
            }
        }
    }

    /**
     * Usuwa słuchaczy pól
     * @see BoardView#reset
     */
    @Override
    public void reset() {
        super.reset();
        placeShip(null);
        removeFieldListeners();
    }

    /**
     * Uaktualnia podgląd aktualnego statku dla podanych współrzędnych.
     * @param coordinates współrzędne początku
     * @param preview <code>true</code> wyświetla podgląd,
     *                w przeciwnym wypadku usuwa
     */
    protected void updatePreview(Coordinates coordinates, boolean preview) {
        if (orientation == Orientation.HORIZONTAL) {
            for (int x = coordinates.getX();
                 x < coordinates.getX() + shipType.getLength()
                         && x < Board.SIZE;
                 ++x) {
                Field field = fieldTable[x][coordinates.getY()];
                field.setPreviewShip(preview);
                field.redraw();
            }
        } else {
            for (int y = coordinates.getY();
                 y < coordinates.getY() + shipType.getLength()
                         && y < Board.SIZE;
                 ++y) {
                Field field = fieldTable[coordinates.getX()][y];
                field.setPreviewShip(preview);
                field.redraw();
            }
        }
    }

    /**
     * Wyświetla podgląd aktualnego statku dla podanych współrzędnych.
     * @param coordinates współrzędne początka statku
     */
    protected void showPreview(Coordinates coordinates) {
        updatePreview(coordinates, true);
    }

    /**
     * Usuwa podgląd aktualnego statku dla podanych współrzędnych.
     * @param coordinates współrzędne początku statku
     */
    protected void removePreview(Coordinates coordinates) {
        updatePreview(coordinates, false);
    }

    /**
     * Słuchacz pól planszy gracza.
     * Gdy nastąpi kliknięcie wysyła prośbę o rozmieszczenie statku.
     * Gdy myszka wejdzie w obszar konkretnego pola wyświetla odpowiednio podgląd.
     * Gdy myszka opuści obszar pola podgląd jest usuwany.
     */
    protected class FieldListener extends MouseAdapter {

        protected Coordinates coordinates;
        protected Protocol protocol;

        /**
         * Tworzy obiekt słuchacza powiązany z podanymi współrzędnymi.
         * @param coordinates współrzędne nasłuchiwanego pola
         * @param protocol protokół do wysłania prośby o umieszczenie statku
         */
        public FieldListener(Coordinates coordinates,
                             Protocol protocol) {
            this.coordinates = coordinates;
            this.protocol = protocol;
        }

        /**
         * Obsługuje kliknięcie myszy.
         * Gdy nastąpi kliknięcie pierwszym przyciskiem zostaje wysłana
         * prośba o umieszczenie statku.
         * Gdy nastąpi kliknięcie trzecim przyciskiem zmieniona zostaje
         * orientacja statku.
         */
        @Override
        public void mouseClicked(MouseEvent e) {
            if (shipType != null) {
                if (e.getButton() == MouseEvent.BUTTON3) {
                    removePreview(coordinates);
                    orientation = orientation.toggle();
                    showPreview(coordinates);
                } else if (e.getButton() == MouseEvent.BUTTON1) {
                    removePreview(coordinates);
                    protocol.sendEvent(
                            new PlaceShipEvent(
                                    shipType, orientation, coordinates));
                }
            }
        }

        /**
         * Po wejściu kursora na pole wyświetlany jest odpowiedni podgląd.
         */
        @Override
        public void mouseEntered(MouseEvent e) {
            if (shipType != null) {
                showPreview(coordinates);
            }
        }

        /**
         * Po wyjściu kursora odpowiedni podgląd jest usuwany.
         */
        @Override
        public void mouseExited(MouseEvent e) {
            if (shipType != null) {
                removePreview(coordinates);
            }
        }
    }
}