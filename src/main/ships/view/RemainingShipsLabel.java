package ships.view;

import java.util.Map;
import java.util.TreeMap;
import javax.swing.JLabel;
import ships.model.ShipType;

/**
 * Klasa służąca do wyświetlania pozostałych statków przeciwnika.
 */
public class RemainingShipsLabel {

    protected JLabel label;
    /**
     * Mapa długość statku-ile pozostało statków tej długości do zniszczenia.
     */
    protected Map<Integer, Integer> shipLengthRemainingShipsMap;

    /**
     * Tworzy etykietę z napisem dla początkowego stanu gry.
     */
    public RemainingShipsLabel() {
        label = new JLabel();
        reset();
    }

    /**
     * Zwraca etykietę.
     */
    public JLabel getLabel() {
        return label;
    }

    /**
     * Rejestruje zniszczenie statku.
     * @param shipType typ statku
     */
    public void registerDestroyedShip(ShipType shipType) {
        Integer length = shipType.getLength();
        Integer numOfShipType = shipLengthRemainingShipsMap.get(length);
        shipLengthRemainingShipsMap.put(
                length, (numOfShipType == null) ? 0 : numOfShipType - 1);
        update();
    }

    /**
     * Przywraca etykietę do stanu początkowego.
     */
    public void reset() {
        shipLengthRemainingShipsMap = new TreeMap<>();
        for (ShipType shipType : ShipType.values()) {
            Integer length = shipType.getLength();
            Integer numOfShipType = shipLengthRemainingShipsMap.get(length);
            shipLengthRemainingShipsMap.put(
                    length, (numOfShipType == null) ? 1 : numOfShipType + 1);
        }
        update();
    }

    /**
     * Ustawia tekst etykiety odzwierciedlający aktualny stan.
     */
    protected void update() {
        StringBuilder stringBuilder = new StringBuilder(
                "Pozostałe statki do zniszczenia | ");
        for (Map.Entry<Integer, Integer> entry :
                shipLengthRemainingShipsMap.entrySet()) {
            int shipLength = entry.getKey();
            int remainingShips = entry.getValue();
            String shipName = View.getNameForShipLength(shipLength);
            stringBuilder.append(shipName + ": " + remainingShips + " | ");
        }
        label.setText(stringBuilder.toString());
    }
}
