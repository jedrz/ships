package ships.view;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Map;
import java.util.TreeMap;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JPanel;
import ships.model.ShipType;

/**
 * Klasa tworząca panel ze statkami do rozmieszczenia.
 */
public class ShipTypesPanel {

    protected final JPanel panel;
    protected final Map<ShipType, JButton> shipTypeButtonMap;

    /**
     * Tworzy panel z przyciskami z rodzajami statków do rozmieszczenia.
     * @param view obiekt widoku
     */
    public ShipTypesPanel(final View view) {
        panel = new JPanel();
        BoxLayout layout = new BoxLayout(panel, BoxLayout.PAGE_AXIS);
        panel.setLayout(layout);

        shipTypeButtonMap = new TreeMap<>();
        for (ShipType shipType : ShipType.values()) {
            String name = View.getNameForShipType(shipType);
            JButton button = new JButton(name);
            button.setAlignmentX(Component.CENTER_ALIGNMENT);
            button.addActionListener(
                    new ShipTypeButtonListener(shipType, view));
            panel.add(button);
            shipTypeButtonMap.put(shipType, button);
            // Dodaj przerwę między przyciskami.
            panel.add(Box.createRigidArea(new Dimension(0, 5)));
        }
    }

    /**
     * Zwraca panel.
     */
    public JPanel getPanel() {
        return panel;
    }

    /**
     * Deaktywuje przycisk związany z podanym typem statku.
     */
    public void disableButton(ShipType shipType) {
        JButton button = shipTypeButtonMap.get(shipType);
        button.setEnabled(false);
    }

    /**
     * Przywraca stan początkowy panelu z rodzajami statków.
     */
    public void reset() {
        for (JButton button : shipTypeButtonMap.values()) {
            button.setEnabled(true);
        }
    }

    /**
     * Słuchacz kliknięć przycisku.
     * Kliknięcie oznacza chęć ustawienia wybranego statku.
     */
    protected class ShipTypeButtonListener implements ActionListener {

        final ShipType shipType;
        final View view;

        public ShipTypeButtonListener(ShipType shipType, final View view) {
            this.shipType = shipType;
            this.view = view;
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            view.placeShip(shipType);
        }
    }
}
