package ships.view;

import java.util.HashMap;
import java.util.Map;
import java.util.Observable;
import java.util.Observer;
import java.util.TreeMap;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import ships.controller.ConnectionErrorHandler;
import ships.controller.Protocol;
import ships.events.AllShipsPlacedEvent;
import ships.events.ControllerEvent;
import ships.events.LostGameEvent;
import ships.events.ModelChangeEvent;
import ships.events.NewGameAgreeEvent;
import ships.events.NewGameRejectEvent;
import ships.events.NewGameRejectReceivedEvent;
import ships.events.NewGameRequestReceivedEvent;
import ships.events.OpponentTurnEvent;
import ships.events.PlaceShipSuccessfulEvent;
import ships.events.PlayerTurnEvent;
import ships.events.StartGameEvent;
import ships.events.WinGameEvent;
import ships.model.ModelChange;
import ships.model.ShipType;

/**
 * Klasa widoku.
 * Jest obserwatorem protokołu, aby otrzymywać zdarzenia od kontrolera.
 */
public class View implements Observer {

    protected Protocol protocol;
    protected Map<Class<? extends ControllerEvent>, EventHandler> eventHandlerMap;
    protected GameWindow gameWindow;
    protected ConnectionErrorHandler connectionErrorHandler;

    /**
     * Mapa typ statku-nazwa.
     */
    protected static final Map<ShipType, String> shipTypeNameMap;
    static {
        shipTypeNameMap = new TreeMap<>();
        shipTypeNameMap.put(ShipType.FOUR_MASTED, "Czteromasztowiec");
        shipTypeNameMap.put(ShipType.THREE_MASTED_1, "Trzymasztowiec 1");
        shipTypeNameMap.put(ShipType.THREE_MASTED_2, "Trzymasztowiec 2");
        shipTypeNameMap.put(ShipType.TWO_MASTED_1, "Dwumasztowiec 1");
        shipTypeNameMap.put(ShipType.TWO_MASTED_2, "Dwumasztowiec 2");
        shipTypeNameMap.put(ShipType.TWO_MASTED_3, "Dwumasztowiec 3");
        shipTypeNameMap.put(ShipType.ONE_MASTED_1, "Jednomasztowiec 1");
        shipTypeNameMap.put(ShipType.ONE_MASTED_2, "Jednomasztowiec 2");
        shipTypeNameMap.put(ShipType.ONE_MASTED_3, "Jednomasztowiec 3");
        shipTypeNameMap.put(ShipType.ONE_MASTED_4, "Jednomasztowiec 4");
    }

    /**
     * Mapa długość statku-nazwa.
     */
    protected static final Map<Integer, String> shipLengthNameMap;
    static {
        shipLengthNameMap = new TreeMap<>();
        shipLengthNameMap.put(4, "Czteromasztowiec");
        shipLengthNameMap.put(3, "Trzymasztowiec");
        shipLengthNameMap.put(2, "Dwumasztowiec");
        shipLengthNameMap.put(1, "Jednomasztowiec");
    }

    /**
     * Zwraca nazwę dla podanego typu statku.
     */
    public static String getNameForShipType(ShipType shipType) {
        String name = shipTypeNameMap.get(shipType);
        return (name != null) ? name : shipType.toString();
    }

    /**
     * Zwraca nazwę dla podanej długości statku
     */
    public static String getNameForShipLength(int length) {
        return shipLengthNameMap.get(length);
    }

    /**
     * Tworzy obiekt widoku - mapę do reakcji na zdarzenia od kontrolera.
     */
    public View(Protocol protocol) {
        this.protocol = protocol;
        connectionErrorHandler = new MyConnectionErrorHandler();
        fillEventHandlerMap();
        showFrame();
    }

    /**
     * Zwraca obiekt do obsługi błędu połączenia.
     */
    public ConnectionErrorHandler getConnectionErrorHandler() {
        return connectionErrorHandler;
    }

    /**
     * Wyświetla główne okno.
     */
    protected void showFrame() {
        final View view = this;
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                gameWindow = new GameWindow(view, protocol);
                gameWindow.showFrame();
            }
        });
    }

    /**
     * Reaguje na zdarzenie otrzymane od serwera.
     */
    @Override
    public void update(Observable obj, Object arg) {
        if (obj instanceof Protocol && arg instanceof ControllerEvent) {
            ControllerEvent event = (ControllerEvent) arg;
            EventHandler eventHandler = eventHandlerMap.get(event.getClass());
            if (eventHandler != null) {
                eventHandler.updateView(event);
            }
        }
    }

    /**
     * Umożliwia ustawienie statku oraz wyświetla podgląd bieżącego ustawienia.
     * @param shipType typ statku
     */
    public void placeShip(final ShipType shipType) {
        gameWindow.getPlayerBoardView().placeShip(shipType);
    }

    protected void fillEventHandlerMap() {
        eventHandlerMap = new HashMap<>();

        // Wyświetla dwie plansze oraz przyciski do ustawiania statków.
        EventHandler startGameEventHandler = new EventHandler() {
            @Override
            public void updateView(ControllerEvent controllerEvent) {
                SwingUtilities.invokeLater(new Runnable() {
                    @Override
                    public void run() {
                        gameWindow.getPlayerBoardView().reset();
                        gameWindow.getPlayerBoardView().addFieldListeners(
                                protocol);
                        gameWindow.getOpponentBoardView().reset();
                        gameWindow.getOpponentBoardView().addFieldListeners(
                                protocol);
                        gameWindow.getShipTypesPanel().reset();
                        gameWindow.getHistoryList().reset();
                        gameWindow.getRemainingShipsLabel().reset();
                        gameWindow.setStatus(
                                "Rozmieść swoje statki na lewej planszy");
                    }
                });
            }
        };
        eventHandlerMap.put(StartGameEvent.class, startGameEventHandler);

        // Obsługuje pomyślne ustawienie statku.
        // Deaktywuje przycisk umożliwiający ponowne ustawienie tego samego
        // statku oraz usuwa podgląd.
        EventHandler placeShipSuccessfulEventHandler = new EventHandler() {
            @Override
            public void updateView(ControllerEvent controllerEvent) {
                PlaceShipSuccessfulEvent event =
                        (PlaceShipSuccessfulEvent) controllerEvent;
                final ShipType shipType = event.getShipType();
                SwingUtilities.invokeLater(new Runnable() {
                    @Override
                    public void run() {
                        gameWindow.getShipTypesPanel().disableButton(shipType);
                        gameWindow.getPlayerBoardView().placeShip(null);
                    }
                });
            }
        };
        eventHandlerMap.put(PlaceShipSuccessfulEvent.class,
                placeShipSuccessfulEventHandler);

        // Usuwa panel z typami statków.
        EventHandler allShipsPlacedEventHandler = new EventHandler() {
            @Override
            public void updateView(ControllerEvent controllerEvent) {
                SwingUtilities.invokeLater(new Runnable() {
                    @Override
                    public void run() {
                        gameWindow.getPlayerBoardView().removeFieldListeners();
                        gameWindow.setStatus(
                                "Przeciwnik rozmieszcza jeszcze swoje statki");
                    }
                });
            }
        };
        eventHandlerMap.put(AllShipsPlacedEvent.class,
                allShipsPlacedEventHandler);

        // Uaktualnia widok planszy.
        EventHandler modelChangeEventHandler = new EventHandler() {
            @Override
            public void updateView(ControllerEvent controllerEvent) {
                ModelChangeEvent event = (ModelChangeEvent) controllerEvent;
                final ModelChange modelChange = event.getModelChange();
                SwingUtilities.invokeLater(new Runnable() {
                    @Override
                    public void run() {
                        gameWindow.getPlayerBoardView().update(
                                modelChange.getPlayerBoardChangeList(),
                                modelChange.getPlayerShipList());
                        gameWindow.getOpponentBoardView().update(
                                modelChange.getOpponentBoardChangeList(),
                                modelChange.getOpponentShipList());

                        if (modelChange.getChangeType() != null) {
                            HistoryList historyList = gameWindow.getHistoryList();
                            switch (modelChange.getChangeType()) {
                                case PLAYER_BOARD_CHANGE:
                                    historyList.addPlayerBoardChange(
                                            modelChange.getPlayerBoardChange());
                                    break;
                                case OPPONENT_BOARD_CHANGE:
                                    historyList.addOpponentBoardChange(
                                            modelChange.getOpponentBoardChange());
                                    break;
                                case PLAYER_SHIP_DESTROYED:
                                    historyList.addPlayerBoardChange(
                                            modelChange.getPlayerBoardChange());
                                    historyList.addPlayerShipDestroyed(
                                            modelChange.getPlayerShipChange());
                                    break;
                                case OPPONENT_SHIP_DESTROYED:
                                    historyList.addOpponentBoardChange(
                                            modelChange.getOpponentBoardChange());
                                    historyList.addOpponentShipDestroyed(
                                            modelChange.getOpponentShipChange());
                                    break;
                            }

                            RemainingShipsLabel remainingShipsLabel =
                                    gameWindow.getRemainingShipsLabel();
                            for (ModelChange.ShipEntry shipEntry :
                                    modelChange.getOpponentShipList()) {
                                ShipType shipType = shipEntry.getShipType();
                                remainingShipsLabel.registerDestroyedShip(
                                        shipType);
                            }
                        }
                    }
                });
            }
        };
        eventHandlerMap.put(ModelChangeEvent.class, modelChangeEventHandler);

        // Informuje gracza o jego ruchu.
        EventHandler playerTurnEventHandler = new EventHandler() {
            @Override
            public void updateView(ControllerEvent controllerEvent) {
                SwingUtilities.invokeLater(new Runnable() {
                    @Override
                    public void run() {
                        gameWindow.setStatus("Twój ruch");
                        gameWindow.getOpponentBoardView().setPlayerTurn(true);
                    }
                });
            }
        };
        eventHandlerMap.put(PlayerTurnEvent.class, playerTurnEventHandler);

        // Informuje gracza o ruchu przeciwnika.
        EventHandler opponentTurnEventHandler = new EventHandler() {
            @Override
            public void updateView(ControllerEvent controllerEvent) {
                SwingUtilities.invokeLater(new Runnable() {
                    @Override
                    public void run() {
                        gameWindow.setStatus("Ruch przeciwnika");
                        gameWindow.getOpponentBoardView().setPlayerTurn(false);
                    }
                });
            }
        };
        eventHandlerMap.put(OpponentTurnEvent.class, opponentTurnEventHandler);

        // Obsługuje wygranie gry przez gracza.
        EventHandler winGameEventHandler = new EventHandler() {
            @Override
            public void updateView(ControllerEvent controllerEvent) {
                SwingUtilities.invokeLater(new Runnable() {
                    @Override
                    public void run() {
                        gameWindow.getOpponentBoardView().removeFieldListeners();
                        gameWindow.setStatus("Wygrana!");
                        JOptionPane.showMessageDialog(
                                gameWindow.getFrame(),
                                "Wygrana!",
                                "Wygrana",
                                JOptionPane.PLAIN_MESSAGE);
                    }
                });
            }
        };
        eventHandlerMap.put(WinGameEvent.class, winGameEventHandler);

        // Obsługuje przegranie gry przez gracza.
        EventHandler lostGameEventHandler = new EventHandler() {
            @Override
            public void updateView(ControllerEvent controllerEvent) {
                SwingUtilities.invokeLater(new Runnable() {
                    @Override
                    public void run() {
                        gameWindow.getOpponentBoardView().removeFieldListeners();
                        gameWindow.setStatus("Przegrana");
                        JOptionPane.showMessageDialog(
                                gameWindow.getFrame(),
                                "Przegrana",
                                "Przegrana",
                                JOptionPane.PLAIN_MESSAGE);
                    }
                });
            }
        };
        eventHandlerMap.put(LostGameEvent.class, lostGameEventHandler);

        // Obsługuje prośbę o chęć nowej gry.
        EventHandler newGameRequestReceivedEventHandler = new EventHandler() {
            @Override
            public void updateView(ControllerEvent controllerEvent) {
                SwingUtilities.invokeLater(new Runnable() {
                    @Override
                    public void run() {
                        int n = JOptionPane.showConfirmDialog(
                                gameWindow.getFrame(),
                                "Wysłano prośbę o nową grę. Zgadzasz się?",
                                "Prośba o nową grę",
                                JOptionPane.YES_NO_OPTION);
                        if (n == 0) {
                            protocol.sendEvent(new NewGameAgreeEvent());
                        } else {
                            protocol.sendEvent(new NewGameRejectEvent());
                        }
                    }
                });
            }
        };
        eventHandlerMap.put(NewGameRequestReceivedEvent.class,
                newGameRequestReceivedEventHandler);

        // Obsługuje niezgodę drugiego gracza na rozpoczęcie gry od nowa.
        EventHandler newGameRejectReceivedEventHandler = new EventHandler() {
            @Override
            public void updateView(ControllerEvent controllerEvent) {
                SwingUtilities.invokeLater(new Runnable() {
                    @Override
                    public void run() {
                        JOptionPane.showMessageDialog(
                                gameWindow.getFrame(),
                                "Gracz odmówił rozpoczęcia gry od nowa",
                                "Odmowa nowej gry",
                                JOptionPane.WARNING_MESSAGE);
                    }
                });
            }
        };
        eventHandlerMap.put(NewGameRejectReceivedEvent.class,
                newGameRejectReceivedEventHandler);
    }

    /**
     * Interfejs obiektu do reakcji na zdarzenie kontrolera.
     */
    protected interface EventHandler {

        void updateView(ControllerEvent controllerEvent);
    }

    /**
     * Klasa do obsługi wyjątków zgłaszanych z powodu problemów z obsługą połączenia.
     */
    protected class MyConnectionErrorHandler
            implements ConnectionErrorHandler {

        @Override
        public void uncaughtException(Throwable e) {
            // Wyświetla okienko i wyłącza program.
            SwingUtilities.invokeLater(new Runnable() {
                @Override
                public void run() {
                    JOptionPane.showMessageDialog(
                            gameWindow.getFrame(),
                            "Brak łączności z drugim graczem",
                            "Problem z połączeniem",
                            JOptionPane.ERROR_MESSAGE);
                    System.exit(1);
                }
            });
        }
    }
}
