package ships;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import ships.model.ModelTest;

@RunWith(Suite.class)
@Suite.SuiteClasses({
        ModelTest.class
})
public class AllTests {
}
