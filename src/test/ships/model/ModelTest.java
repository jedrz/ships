package ships.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import org.junit.Test;

public class ModelTest {

    @Test
    public void testPlacingShips() {
        Model model = new Model();
        int shipsPlaced = 0;
        int y = 0;
        for (ShipType shipType : ShipType.values()) {
            while (true) {
                Coordinates coordinates = null;
                try {
                    coordinates = new Coordinates(0, y);
                } catch (InvalidCoordinateException e) {
                    e.printStackTrace();
                }
                boolean res1 = model.placeLocalPlayerShip(
                        shipType, Orientation.HORIZONTAL, coordinates);
                boolean res2 = model.placeRemotePlayerShip(
                        shipType, Orientation.HORIZONTAL, coordinates);
                if (res1 && res2) {
                    ++shipsPlaced;
                    break;
                }
                ++y;
            }
        }
        assertEquals(shipsPlaced, ShipType.values().length);
    }

    @Test
    public void placeShipsExceedingBoard() {
        Model model = new Model();
        for (ShipType shipType : ShipType.values()) {
            Coordinates coordinatesHorizontal = null;
            Coordinates coordinatesVertical = null;
            try {
                coordinatesHorizontal = new Coordinates(
                        Board.SIZE - shipType.getLength() + 1,
                        Board.SIZE - 1);
                coordinatesVertical = new Coordinates(
                        Board.SIZE - 1,
                        Board.SIZE - shipType.getLength() + 1);
            } catch (InvalidCoordinateException e) {
                // Pomiń jednomasztowca.
                continue;
            }
            boolean placeHorizontalResult = model.placeLocalPlayerShip(
                    shipType, Orientation.HORIZONTAL, coordinatesHorizontal);
            assertFalse(placeHorizontalResult);
            boolean placeVerticalResult = model.placeLocalPlayerShip(
                    shipType, Orientation.VERTICAL, coordinatesVertical);
            assertFalse(placeVerticalResult);
        }
    }
}
